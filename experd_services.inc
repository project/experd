<?php

/**
 * @file
 * Include file for experd webservices implementation.
 * In this file all the callback functions for the ExperD webservices API are implemented.
 * @see `experd_services_resources()` for the definitons of the API.
 */

/**
 * Callback function for experd index webservice.
 *
 * Returns a list of all ExperD nodes the current user has access to.
 *
 */
function _experd_index($page, $fields, $parameters, $page_size) {
  module_load_include('inc', 'services', 'services.module');
  $node_select = db_select('node', 't')
          ->condition('type', 'experd', '=')
          ->addTag('node_access')
          ->orderBy('created', 'ASC');

  services_resource_build_index_query($node_select, $page, $fields, $parameters, $page_size, 'node');

  if (!user_access('administer nodes')) {
    $node_select->condition('status', 1);
  }

  $results = services_resource_execute_index_query($node_select);

  return services_resource_build_index_list($results, 'experd', 'nid');
}

/**
 * Callback function for experd retrieve.
 *
 * Returns the selected ExperD node.
 *
 * @param int $nid
 * Node id of ExperD to retrieve.
 */
function _experd_retrieve($nid) {
  // Check for int values
  $nid = intval($nid);

  $node = node_load($nid);
  $node_wrapper = entity_metadata_wrapper('node', $node);
  if ($node && $node->type == 'experd') {
    //cleanup node

    if (!empty($node_wrapper->field_xd_description)) {
      try {
        $node->description = $node_wrapper->field_xd_description->value->value(array('sanitize' => TRUE));
      } catch (EntityMetadataWrapperException $exc) {
        $node->description = '';
      }
    }
    unset($node->field_xd_description);

    $node->courses = array();
    if (!empty($node_wrapper->field_xd_courses)) {
      foreach ($node_wrapper->field_xd_courses as $course_wrapper) {
        $node->courses[] = array('name' => $course_wrapper->label(), 'tid' => $course_wrapper->getIdentifier());
      }
    }
    unset($node->field_xd_courses);

    $node->operations = array();
    if (!empty($node_wrapper->field_xd_operations)) {
      foreach ($node_wrapper->field_xd_operations as $operation_wrapper) {
        $operation = _experd_load_operation($operation_wrapper);
        $node->operations[$operation_wrapper->getIdentifier()] = $operation;
      }
    }
    unset($node->field_xd_operations);

    $node->freeze_workflow = $node_wrapper->field_xd_freeze_workflow->value() == 1;
    unset($node->field_xd_freeze_workflow);
    $node->enable_status = $node_wrapper->field_xd_enable_status->value() == 1;
    unset($node->field_xd_enable_status);

    //remove user and group info from webservice
    unset($node->field_xd_users);
    unset($node->og_group_ref);
    unset($node->group_content_access);

    _experd_remove_unneeded_node_fields($node);
  }
  return $node;
}

/**
 * Callback function for experd operations.
 *
 * Returns all operations for selected ExperD node.
 *
 * @param int $nid
 * Node id of selected ExperD.
 */
function _experd_getoperations($nid) {
  $nid = intval($nid);

  $node_wrapper = entity_metadata_wrapper('node', $nid);
  if ($node_wrapper && $node_wrapper->getBundle() == 'experd') {
    //cleanup operations
    $operations = array();
    if (!empty($node_wrapper->field_xd_operations)) {
      foreach ($node_wrapper->field_xd_operations as $operation_wrapper) {
        $operation = _experd_load_operation($operation_wrapper);
        $operations[$operation_wrapper->getIdentifier()] = $operation;
      }
    }
  }
  return $operations;
}

/**
 * Load data for an operation.
 *
 * @param $operation_wrapper
 * An entity_metadata_wrapper() for the selected experd_operation node.
 * @return stdClass
 * Data for the selected operation.
 */
function _experd_load_operation($operation_wrapper) {
  $newoperation = new stdClass();
  if ($operation_wrapper->getBundle() == 'experd_operation') {
    //cleanup operation and format as webservice return value
    $newoperation->name = $operation_wrapper->label();
    $newoperation->nid = $operation_wrapper->getIdentifier();
    //get category name and tid from taxonomy
    $newoperation->classification = array();
    if (!empty($operation_wrapper->field_xd_classification)) {
      $newoperation->classification = array(
          'name' => $operation_wrapper->field_xd_classification->label(),
          'tid' => $operation_wrapper->field_xd_classification->getIdentifier()
      );
    }
    //get imageurl if set
    if (!empty($operation_wrapper->field_xd_image)) {
      $image = $operation_wrapper->field_xd_image->value();
      $newoperation->imageUrl = $image['uri'] != '' ? file_create_url($image['uri']) : false;
    }
    // get labbuddy method list
    if (!empty($operation_wrapper->field_xd_labbuddy_methods)) {
      $newoperation->labbuddy_method = $operation_wrapper->field_xd_labbuddy_methods->raw();
    }
    // get description
    if (!empty($operation_wrapper->field_xd_operation_description)) {
      try {
        $newoperation->description = $operation_wrapper->field_xd_operation_description->value(); //->value(array('sanitize' => TRUE));
      } catch (EntityMetadataWrapperException $exc) {
        $newoperation->description = '';
      }
    }
    // get info url
    if (!empty($operation_wrapper->field_xd_more_info_url)) {
      try {
        $newoperation->more_info_url = $operation_wrapper->field_xd_more_info_url->value(); //->value(array('sanitize' => TRUE));
      } catch (EntityMetadataWrapperException $exc) {
        $newoperation->more_info_url = '';
      }
    }
    // get ports list
    if (!empty($operation_wrapper->field_xd_ports)) {
      $newoperation->inputPorts = array();
      $newoperation->outputPorts = array();
      foreach ($operation_wrapper->field_xd_ports as $port_wrapper) {
        $thePort = _experd_load_port($port_wrapper);
        if ($thePort->port_type == "1") {
          $newoperation->inputPorts[] = $thePort;
        }
        else {
          $newoperation->outputPorts[] = $thePort;
        }
      }
    }
  }
  return $newoperation;
}

/**
 * Load data for an ExperD port.
 *
 * @param type $port_wrapper
 * entity_metadata_wrapper for the port
 * @return stdClass
 * Data for the port.
 */
function _experd_load_port($port_wrapper) {
  $port = new stdClass();
  if ($port_wrapper->getBundle() == 'experd_port') {
    //cleanup experd port and format for webservice return value
    $port->name = $port_wrapper->label();
    $port->nid = $port_wrapper->getIdentifier();
    $port->port_type = $port_wrapper->field_xd_port_type->value();
  }
  return $port;
}

/**
 * Get all organic groups for this ExperD with the current user as a member.
 *
 * @param type $nid
 * Node id for ExperD node
 * @return array
 * List of groups
 */
function _experd_get_groups($nid) {
  $groupList = og_get_groups_by_user();

  $node_wrapper = entity_metadata_wrapper('node', $nid);
  // Put group ids for this ExperD and current user in an array
  $expdesGroups = array();
//  if (!empty($node_wrapper->field_xd_user_groups)) {
//    foreach ($node_wrapper->field_xd_user_groups as $group) {
  if (!empty($node_wrapper->og_group_ref)) {
    foreach ($node_wrapper->og_group_ref as $group) {
      if (array_key_exists($group->getIdentifier(), $groupList['node'])) {
        $theGroup = $group->value();
        _experd_remove_unneeded_node_fields($theGroup);
        $theGroup->members = _get_users_in_group($group->getIdentifier());
        $expdesGroups[] = $theGroup;
        //$expdesGroups[] = $group->value();
      }
//      if ($group['target_id'] && array_key_exists($group['target_id'], $groupList['node'])) {
//        $expdesGroupIds[] = $group['target_id'];
//      }
    }
  }
  return $expdesGroups;
}

/*
  function _getusers_in_group($gid) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'user')
  ->fieldCondition('og_user_node', 'target_id', $gid, '=');

  $results = $query->execute();
  }
 */

/**
 * Get the group id the currently logged in user has for the given designer
 * node.
 *
 * @param int $nid
 *   The node id of the ExperD node.
 *
 * @param int $gid
 *   The group or user id of the ExperD (optional).
 *
 * @return int
 *   The group id of the currently selected group for this user.
 */
function get_user_or_group_for_node($nid, $gid) {
  global $user;
  // cache groupId in $_SESSION
  if (isset($_SESSION['experd']) &&
          isset($_SESSION['experd']['#groupid']) &&
          $_SESSION['experd']['#nid'] === $nid &&
          $_SESSION['experd']['#uid'] === $user->uid) {
    return $_SESSION['experd']['#groupid'];
  }
  else {
    $_SESSION['experd'] = array(
        '#nid' => $nid,
        '#uid' => $user->uid,
    );
  }
  $groupId = -1;
  //get list of groups for current user
  $groupList = og_get_groups_by_user();

  $node_wrapper = entity_metadata_wrapper('node', $nid);
  // Put group ids for this ExperD and current user in an array
  $expdesGroupIds = array();
//  if (!empty($node_wrapper->field_xd_user_groups)) {
//    foreach ($node_wrapper->field_xd_user_groups as $group) {
  if (!empty($node_wrapper->og_group_ref)) {
    foreach ($node_wrapper->og_group_ref as $group) {
      if (array_key_exists($group->getIdentifier(), $groupList['node'])) {
        $expdesGroupIds[] = $group->getIdentifier();
      }
    }
  }

  //check if individual user is assigned to this ExperD
  $isvaliduserid = FALSE;
  if (!empty($node_wrapper->field_xd_users)) {
    foreach ($node_wrapper->field_xd_users AS $xpuser) {
      if ($xpuser->getIdentifier() == $user->uid) {
        $isvaliduserid = TRUE;
        break;
      }
    }
  }
  if (isset($gid)) { // $gid is supplied.
    // Check for validity: user is member of this group
    // and ExperD node is assigned to this group.
    if (in_array($gid, $expdesGroupIds)) {
      $groupId = $gid;
    }
    else { //no matching goup found for user
      // try to match user id to current user and ExperD userlist
      if ($isvaliduserid && $user->uid === $gid) {
        $groupId = $gid; // TODO: temporary hack
      }
    }
  }
  else {// $gid is not supplied: find match for current user
    if (count($expdesGroupIds) > 0) {
      // Return first match If user is member of multiple groups and no $gid given.
      $groupId = $expdesGroupIds[0];
    }
    else { //no matching goup found for user
      // match user id to ExperD userlist
      if ($isvaliduserid) {
        $groupId = $user->uid;
      }
    }
  }
  // cache groupId in $_SESSION
  $_SESSION['experd']['#groupid'] = $groupId;
  return intval($groupId);
}

/**
 * Get all users of an organic group
 *
 * @return
 * List of all users (uid, name) of the selected group
 */
function _get_users_in_group($gid) {
  $query = db_select('users', 'u');

  $query
          ->condition('u.uid', 0, '<>')
          ->condition('u.status', 1, '=')
          ->fields('u', array('uid', 'name'))
          ->join('og_membership', 'ogm', "ogm.gid = :gid AND u.uid = ogm.etid AND ogm.entity_type = 'user'", array(':gid' => $gid));
  return $query->execute()->fetchAll();
}

/**
 * Remove all the default node fields that we don't need for the webservice
 * @param type $node
 */
function _experd_remove_unneeded_node_fields(&$node) {
  $fieldList = array(
      'log',
      'status',
      'promote',
      'sticky',
      'tnid',
      'translate',
      'revision_timestamp',
      'revision_uid',
      'rdf_mapping',
      'comment',
      'cid',
      'last_comment_timestamp',
      'last_comment_name',
      'last_comment_uid',
      'comment_count',
      'name',
      'picture',
      'data',
  );
  foreach ($fieldList as $field) {
    unset($node->$field);
  }
}

/**
 * Callback function for saveOperationState webservice.\n
 *
 * Calls one of the following functions, depending on value of "databaseAction" field:
 * - `_experd_saveoperationstate_add()`
 * - `_experd_saveoperationstate_update()`
 * - `_experd_saveoperationstate_delete()`
 *
 * Returns the current operationstates
 *
 * @param int $nid
 * Node id of the ExperD node.
 * @param array $data
 *  array of added, updated or deleted operation states
 * @param int $from_sid
 * Return operation states starting at sid $from_sid
 * @param int $gid
 * @return array
 * states updated since $from_sid
 */
function _experd_saveoperationstate($nid, $data, $from_sid, $gid) {
  global $user;
  $groupId = get_user_or_group_for_node($nid, $gid); //find or validate a group id
  if ($groupId < 0) {
    return services_error(t('Invalid group or user id'), 406);
  }
  $state_list = $data['states'];
  if (!schema_is_readonly($nid, $groupId)) {
    $len = sizeof($state_list);
    for ($i = 0; $i < $len; $i++) {
      $action = 'update';
      if (isset($state_list[$i]['databaseAction'])) {
        $action = $state_list[$i]['databaseAction'];
      }
      switch ($action) {
        case 'add': // add state
          _experd_saveoperationstate_add($nid, $groupId, $user->uid, $state_list[$i]);
          break;
        case 'delete': // delete state
          _experd_saveoperationstate_delete($nid, $groupId, $user->uid, $state_list[$i]);
          break;

        default: // update state
          _experd_saveoperationstate_update($nid, $groupId, $user->uid, $state_list[$i]);
          break;
      }
    }
    // return states updated since $from_sid
    return _experd_getStatesFromSid($nid, $from_sid, $groupId);
  }
  else {
    // return all states
    return _experd_getStatesFromSid($nid, 0, $groupId);
  }
}

/**
 * Save a new operation state.
 *
 * Called from `_experd_saveoperationstate()`
 *
 * @param int $nid
 *   Node id of the ExperD node.
 * @param int $groupId
 *   Group id this operation state belongs to.
 * @param int $userId
 *   User id of user adding this operation state.
 * @param array $data
 *   data for the added operation state
 */
function _experd_saveoperationstate_add($nid, $groupId, $userId, $data) {
  // get uuid
  $uuid = $data['operationJsNid'];
  // insert new state into lb_state_history table
  // returns new sid
  $sid = db_insert('lb_state_history')
          ->fields(array(
              'operation_uuid' => $uuid,
              'entity_nid' => $nid,
              'operation_nid' => $data['operationDbNid'],
              'gid' => $groupId,
              'uid' => $userId,
              'timestamp' => REQUEST_TIME,
          ))
          ->execute();
  // insert new state into lb_state table
  $operation_uuid = db_insert('lb_state')
          ->fields(array(
              'operation_uuid' => $uuid,
              'sid' => $sid,
              'entity_nid' => $nid,
              'operation_nid' => $data['operationDbNid'],
              'gid' => $groupId,
              'uid' => $userId,
              'timestamp' => REQUEST_TIME,
          ))
          ->execute();

  // handle plugins, experd is default plugin
  $pluginID = 'experd'; // experd is the main plugin
  $pluginData = $data['plugins'][$pluginID];

  // insert new state in lb_experd_state table
  $fk_sid = db_insert('lb_experd_state')
          ->fields(array(
              'operation_uuid' => $uuid,
              'fk_sid' => $sid,
              'action' => $pluginData['action'],
              'location' => isset($pluginData['location']) ? drupal_json_encode($pluginData['location']) : '{}',
              'dimensions' => isset($pluginData['dimensions']) ? drupal_json_encode($pluginData['dimensions']) : '{}',
              'connections' => isset($pluginData['connectionsOut']) ? drupal_json_encode($pluginData['connectionsOut']) : '[]',
          ))
          ->execute();

  // copy new state to experd plugin history
  $result = db_query("INSERT INTO {lb_experd_state_history} (operation_uuid, action, location, dimensions, connections, fk_sid) "
          . "SELECT operation_uuid, action, location, dimensions, connections, fk_sid "
          . "FROM {lb_experd_state} WHERE operation_uuid = :uuid", array(':uuid' => $uuid));

  /**
   * Call `hook_saveoperationstate_add()` to allow plugins to add this operation to their database tables
   */
  module_invoke_all('saveoperationstate_add', $sid, $uuid, $data);
}

/**
 * Delete an operation state
 *
 * Called from `_experd_saveoperationstate()`
 *
 * @param int $nid
 *   Node id of the ExperD node.
 * @param int $groupId
 *   Group id this operation state belongs to.
 * @param int $userId
 *   User id of user deleting this operation state.
 * @param array $data
 *   data for the deleted operation state
 */
function _experd_saveoperationstate_delete($nid, $groupId, $userId, $data) {
  // get uuid
  $uuid = $data['operationJsNid'];
  // insert into state history, returns new sid
  $sid = db_insert('lb_state_history')
          ->fields(array(
              'operation_uuid' => $uuid,
              'entity_nid' => $nid,
              'operation_nid' => $data['operationDbNid'],
              'gid' => $groupId,
              'uid' => $userId,
              'deleted' => 1,
              'timestamp' => REQUEST_TIME,
          ))
          ->execute();

  // delete state from lb_state
  $num_deleted = db_delete('lb_state ')
          ->condition('operation_uuid', $uuid, '=')
          ->execute();

  // handle plugins, experd is default plugin
  $pluginID = 'experd'; // experd is the main plugin
  if (isset($data['plugins'][$pluginID])) {
    $pluginData = $data['plugins'][$pluginID];

    // delete state from lb_experd_state
    $num_deleted = db_delete('lb_experd_state ')
            ->condition('operation_uuid', $uuid, '=')
            ->execute();

    if ($num_deleted > 0) {
      // insert deleted state into experd plugin history
      $fk_sid = db_insert('lb_experd_state_history')
              ->fields(array(
                  'operation_uuid' => $uuid,
                  'fk_sid' => $sid,
                  'action' => $pluginData['action'],
              ))
              ->execute();
    }
  }
  /**
   * Call `hook_saveoperationstate_delete()` to allow plugins to delete this operation from their database tables
   */
  module_invoke_all('saveoperationstate_delete', $sid, $uuid, $data);
}

/**
 * Update an operation state
 *
 * Called from `_experd_saveoperationstate()`
 *
 * @param int $nid
 *   Node id of the ExperD node.
 * @param int $groupId
 *   Group id this operation state belongs to.
 * @param int $userId
 *   User id of user updated this operation state.
 * @param array $data
 *   data for the updated operation state
 */
function _experd_saveoperationstate_update($nid, $groupId, $userId, $data) {
  // get uuid
  $uuid = $data['operationJsNid'];
  // insert into state history, returns new sid
  $sid = db_insert('lb_state_history')
          ->fields(array(
              'operation_uuid' => $uuid,
              'entity_nid' => $nid,
              'operation_nid' => $data['operationDbNid'],
              'gid' => $groupId,
              'uid' => $userId,
              'timestamp' => REQUEST_TIME,
          ))
          ->execute();

  // update state sid to new sid
  $num_updated = db_update('lb_state')
          ->fields(array(
              'sid' => $sid,
              'timestamp' => REQUEST_TIME,
          ))
          ->condition('operation_uuid', $uuid, '=')
          ->execute();

  // handle plugins, experd is default plugin
  $pluginID = 'experd'; // experd is the main plugin
  if (isset($data['plugins'][$pluginID])) {
    $pluginData = $data['plugins'][$pluginID];

    // update experd plugin state
    $updated_fields = array(
        'fk_sid' => $sid,
        'action' => $pluginData['action'],
    );
    // only update changed fields
    if (isset($pluginData['location'])) {
      $updated_fields['location'] = drupal_json_encode($pluginData['location']);
    }
    if (isset($pluginData['dimensions'])) {
      $updated_fields['dimensions'] = drupal_json_encode($pluginData['dimensions']);
    }
    if (isset($pluginData['connectionsOut'])) {
      $updated_fields['connections'] = drupal_json_encode($pluginData['connectionsOut']);
    }

    $num_updated = db_update('lb_experd_state')
            ->fields($updated_fields)
            ->condition('operation_uuid', $uuid, '=')
            ->execute();

    if ($num_updated > 0) {
      // copy state to experd plugin history
      $result = db_query("INSERT INTO {lb_experd_state_history} (operation_uuid, action, location, dimensions, connections, fk_sid) "
              . "SELECT operation_uuid, action, location, dimensions, connections, fk_sid "
              . "FROM {lb_experd_state} WHERE operation_uuid = :uuid", array(':uuid' => $uuid));
    }
  }
  /**
   * Call `hook_saveoperationstate_update()` to allow plugins to update this operation in their database tables
   */
  module_invoke_all('saveoperationstate_update', $sid, $uuid, $data);
}

/**
 * Callback function for getStatesFromSid webservice
 *
 * @param int $nid
 *   ExperD node id
 * @param int $sid
 *   Return states statring witd state id $sid.
 * @param int $gid
 *   group id of group this operation state belongs to.
 * @returns array
 *   Array of all added, updated or deleted operations.
 */
function _experd_getStatesFromSid($nid, $sid, $gid) {
  $groupId = get_user_or_group_for_node($nid, $gid);
  if ($groupId < 0) {
    return services_error(t('Invalid group or user id'), 406);
  }

  $query = db_select('lb_state', 's')
          ->fields('s', array('sid', 'operation_uuid', 'operation_nid'))
          ->condition('sid', $sid, '>=')
          ->condition('gid', $groupId, '=')
          ->condition('entity_nid', $nid, '=')
          ->orderBy('sid', 'DESC');
  $query->leftJoin('lb_experd_state', 'xd', 's.operation_uuid=xd.operation_uuid');
  $query->fields('xd', array('action', 'location', 'dimensions', 'connections'));
  $query->addTag('labbuddy_getstates'); //add tag to allow plugins to change query using hook_query_alter
  $result = $query->execute()
          ->fetchAll();

  // change fields to expected webservice return values
  // decode json strings from database
  $state_list = array();
  $len = sizeof($result);
  for ($i = 0; $i < $len; $i++) {
    $stateData = array(
        'sid' => intval($result[$i]->sid),
        'operationJsNid' => $result[$i]->operation_uuid,
        'operationDbNid' => intval($result[$i]->operation_nid),
        // add plugins namespace
        'plugins' => array(
            // add experd namespace and data
            'experd' => array(
                'location' => drupal_json_decode($result[$i]->location),
                'dimensions' => drupal_json_decode($result[$i]->dimensions),
                'connectionsOut' => drupal_json_decode($result[$i]->connections),
                'action' => $result[$i]->action,
            ),
        ),
    );
    // hook for plugins to handle their data
    drupal_alter('getStatesFromSid', $stateData, $result[$i]);
    $state_list[] = $stateData;
  }

  if ($sid > 0) { // add deleted states if this is not the initial load
    $query = db_select('lb_state_history', 's')
            ->fields('s', array('sid', 'operation_uuid', 'operation_nid'))
            ->condition('sid', $sid, '>=')
            ->condition('gid', $groupId, '=')
            ->condition('entity_nid', $nid, '=')
            ->condition('deleted', 1, '=')
            ->orderBy('sid', 'DESC');
    $deletedStates = $query->execute()
            ->fetchAll();

    $len = sizeof($deletedStates);
    for ($i = 0; $i < $len; $i++) {
      $stateData = array(
          'sid' => intval($deletedStates[$i]->sid),
          'operationJsNid' => $deletedStates[$i]->operation_uuid,
          'operationDbNid' => intval($deletedStates[$i]->operation_nid),
          'databaseAction' => 'delete',
          // add plugins namespace
          'plugins' => array(
              // add experd namespace
              'experd' => array(
                  'action' => 'delete',
              ),
          ),
      );
      // no need for other data, state will be deleted
      $state_list[] = $stateData;
    }
  }
  $response = array(
      'readonly' => schema_is_readonly($nid, $groupId),
      'states' => $state_list,
  );
  return $response;
}

/**
 * Save a snapshot of the current schema.\n
 * Saved in table 'lb_schema_snapshots' as JSON array of sids
 * @param int $nid
 *   ExperD node id
 * @param array $data
 *   Posted data, $data[0] = name
 * @param int $gid
 *   Group id
 * @return int Snapshot id
 */
function _experd_save_schema_snapshot($nid, $data, $gid) {
  global $user;

  $name = $data[0];
  $groupId = get_user_or_group_for_node($nid, $gid);
  if ($groupId < 0) {
    return services_error(t('Invalid group or user id'), 406);
  }

  $query = db_select('lb_state', 's')
          ->fields('s', array('sid'))
          ->condition('gid', $groupId, '=')
          ->condition('entity_nid', $nid, '=')
          ->execute();
  $sids = $query->fetchCol(); //list of all current state id's for this snapshot
  //$sids = implode(',', $sids); // store as comma separated list

  $snap_id = db_insert('lb_schema_snapshots')
          ->fields(array(
              'name' => $name,
              'entity_nid' => $nid,
              'gid' => $groupId,
              'uid' => $user->uid,
              'sids' => json_encode($sids), //store as JSON
              'timestamp' => REQUEST_TIME,
          ))
          ->execute();
  return $snap_id;
}

/**
 * Retrieve all snapshots for ExperD $nid for group $gid
 * @param int $nid
 *   Experd id
 * @param int $gid
 *   Group id
 * @return array
 *   Snapshots
 */
function _experd_get_snapshots($nid, $gid) {
  $groupId = get_user_or_group_for_node($nid, $gid);
  if ($groupId < 0) {
    return services_error(t('Invalid group or user id'), 406);
  }

  $result = db_query("SELECT snap_id, name, timestamp "
          . "FROM {lb_schema_snapshots} "
          . "WHERE entity_nid=:nid AND gid=:group ", array(
      ':nid' => $nid,
      ':group' => $groupId,
  ));
  $snapshots = $result->fetchAll();
  return $snapshots;
}

function _experd_load_snapshot_with_id($nid, $snap_id, $gid) {
  $groupId = get_user_or_group_for_node($nid, $gid);
  if ($groupId < 0) {
    return services_error(t('Invalid group or user id'), 406);
  }

  // get array of state ids saved as snapshot snap_id
  $snapshot = db_select('lb_schema_snapshots', 's')
          ->fields('s')
          ->condition('snap_Id', $snap_id, '=')
          ->execute()
          ->fetchAssoc();
  $sids = json_decode($snapshot['sids']);

  /*
    $sqlQuery = "SELECT s.sid AS sid, s.operation_nid AS operationDbNid, s.operation_uuid AS operationJsNid, xd.action AS action, xd.location AS location, xd.dimensions AS dimensions, xd.connections AS connectionsOut"
    . ", peh.data as propertyeditor_data"
    . " FROM {lb_state_history} s"
    . " LEFT JOIN {lb_experd_state_history} xd ON xd.fk_sid ="
    . "("
    . " SELECT MAX(fk_sid) FROM {lb_experd_state_history} xdh"
    . " WHERE (xdh.operation_uuid = s.operation_uuid)"
    . ")"
    . " LEFT JOIN {lb_propertyeditor_state_history} peh ON peh.fk_sid ="
    . "("
    . " SELECT MAX(fk_sid) FROM {lb_propertyeditor_state_history} peh"
    . " WHERE (peh.operation_uuid = s.operation_uuid)"
    . ")"
    . " WHERE (sid IN (:sids))";

    // select states for this snapshot from lb_state_history
    $query = db_query($sqlQuery, array(':sids' => $sids));
    $result = $query->fetchAll();
   */

  $query = db_select('lb_state_history', 's')
          ->fields('s', array('sid', 'operation_uuid', 'operation_nid'))
          ->condition('sid', $sids, 'IN')
          ->orderBy('sid', 'DESC');

  // Add fields for experd-plugin
  // Join with first state from history with
  // lb_state_history.operation_uuid = lb_experd_state_history.operation_uuid
  // and highest fk_sid <= sid
  $query->leftJoin('lb_experd_state_history', 'xd', "xd.fk_sid=(SELECT MAX(fk_sid) FROM {lb_experd_state_history} xdh "
          . "WHERE xdh.operation_uuid = s.operation_uuid AND xdh.fk_sid <= s.sid)");
  $query->fields('xd', array('action', 'location', 'dimensions', 'connections'));
  //add tag to allow plugins to change query using hook_query_alter
  $query->addTag('labbuddy_getstates_history');

  $result = $query->execute()
          ->fetchAll();


  // change fields to expected webservice return values
  // decode json strings from database
  $state_list = array();
  $len = sizeof($result);
  for ($i = 0; $i < $len; $i++) {
    $stateData = array(
        'sid' => intval($result[$i]->sid),
        'operationJsNid' => $result[$i]->operation_uuid,
        'operationDbNid' => intval($result[$i]->operation_nid),
        // add plugins namespace
        'plugins' => array(
            // add experd namespace
            'experd' => array(
                'location' => drupal_json_decode($result[$i]->location),
                'dimensions' => drupal_json_decode($result[$i]->dimensions),
                'connectionsOut' => drupal_json_decode($result[$i]->connections),
                'action' => $result[$i]->action,
            ),
        ),
    );
    // hook for plugins to handle their data
    drupal_alter('getStatesFromSid', $stateData, $result[$i]);
    $state_list[] = $stateData;
  }

  $response = array(
      'readonly' => schema_is_readonly($nid, $groupId),
      'states' => $state_list,
  );
  return $response;
}

/**
 * Restore a schema snapshot and save as current schema.\n
 * To restore a snapshot:
 * - load all states from state_history.
 * - for each state: add plugin data for all plugins
 * - delete all states from schema
 * - re-add states from snapshot
 * - reload schema
 * @param int $nid
 *   ExperD node id
 * @param int $snap_id
 *   Snapshot id
 * @param boolean $autosave
 *   if TRUE: save a snapshot of current schema as backup
 * @param int $gid
 *   Group id
 * @return array
 *  All operation states for restored schema.
 *
 */
function _experd_restore_snapshot_with_id($nid, $snap_id, $autosave, $gid) {
  $groupId = get_user_or_group_for_node($nid, $gid);
  if ($groupId < 0) {
    return services_error(t('Invalid group or user id'), 406);
  }

  if ($autosave) {
    _experd_save_schema_snapshot($nid, array('autosave'), $groupId);
  }

  //get array of uuids for current schema
  $current_uuids = db_select('lb_state', 's')
          ->fields('s', array('operation_uuid'))
          ->condition('entity_nid', $nid, '=')
          ->condition('gid', $groupId, '=')
          ->execute()
          ->fetchCol();

  //get array of sids for snapshot with snap_id
  $snapshot = db_select('lb_schema_snapshots', 's')
          ->fields('s', array('sids'))
          ->condition('snap_id', $snap_id, '=')
          ->execute()
          ->fetchAssoc();
  $sids = json_decode($snapshot['sids']);

  // get array of uuids for snapshot
  $snap_uuids = db_select('lb_state_history', 's')
          ->fields('s', array('operation_uuid'))
          ->condition('sid', $sids, 'IN')
          ->execute()
          ->fetchCol();

  // get state data for snapshot
  $snapshot_data = _experd_load_snapshot_with_id($nid, $snap_id, $gid);
  // save snapshot states as new schema
  // add or update
  $len = sizeof($snapshot_data['states']);
  for ($i = 0; $i < $len; $i++) {
    $uuid = $snapshot_data['states'][$i]['operationJsNid'];
    $inOldSchema = in_array($uuid, $current_uuids);
    if ($inOldSchema) {
      $action = 'update';
    }
    else {
      $action = 'add';
    }
    $snapshot_data['states'][$i]['databaseAction'] = $action;
    unset($snapshot_data['states'][$i]['sid']);
  }

// find deleted states
  $deleted_uuids = array_diff($current_uuids, $snap_uuids);
  if (count($deleted_uuids) > 0) {
    $deletedStates = db_select('lb_state', 's')
            ->fields('s', array('sid', 'operation_uuid', 'operation_nid'))
            ->condition('operation_uuid', $deleted_uuids, 'IN')
            ->execute()
            ->fetchAll();

// add deleted states to snapshot_data
    $len = sizeof($deletedStates);
    for ($i = 0; $i < $len; $i++) {
      $stateData = array(
          'sid' => intval($deletedStates[$i]->sid),
          'operationJsNid' => $deletedStates[$i]->operation_uuid,
          'operationDbNid' => intval($deletedStates[$i]->operation_nid),
          'databaseAction' => 'delete',
          // add plugins namespace
          'plugins' => array(
              // add experd namespace
              'experd' => array(
                  'action' => 'delete',
              ),
          ),
      );
      // no need for other data, state will be deleted
      $snapshot_data['states'][] = $stateData;
    }
  }
  $result = _experd_saveoperationstate($nid, $snapshot_data, 0, $groupId);
  return $result;
}

/**
 * make schema readonly for students if a snapshot is submitted and
 * option is checked in experd.\n
 * Check permissions to override
 *
 * @param int $nid Experd node id
 * @param int $groupId Group id
 * @return boolean rread-only state
 */
function schema_is_readonly($nid, $groupId) {
  global $user;
  // cache $freeze_submitted_workflow in $_SESSION
  if (isset($_SESSION['experd']) &&
          isset($_SESSION['experd']['#freeze_submitted_workflow'])) {
    $freeze_submitted_workflow = $_SESSION['experd']['#freeze_submitted_workflow'];
  }
  else {
    $freeze_submitted_workflow = FALSE;
    // check if experd should be readonly after submit
    $node = node_load($nid);
    if ($node && $node->type == 'experd') {
      $node_wrapper = entity_metadata_wrapper('node', $node);
      $freeze_submitted_workflow = $node_wrapper->field_xd_freeze_workflow->value();
    }
    // check if user has 'unfreeze submitted workflows' permission
    //$allow_unfreeze = user_access('unfreeze submitted workflows');
    $freeze_submitted_workflow = $freeze_submitted_workflow && !user_access('unfreeze submitted workflows');
    $_SESSION['experd']['#freeze_submitted_workflow'] = $freeze_submitted_workflow;
  }

  // count snapshots, if submitted, a snapshot is created
  $snap_count = db_query('SELECT COUNT(snap_id) FROM {lb_schema_snapshots} '
          . 'WHERE gid = :groupId AND entity_nid = :nid', array(':groupId' => $groupId, ':nid' => $nid))
          ->fetchField();
  $is_submitted = $snap_count > 0;

  $frozen = $is_submitted && $freeze_submitted_workflow;
  return $frozen;
}
