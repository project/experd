(function () {
  // Load angular app after Drupal has initialized the Drupal.settings  property
  Drupal.behaviors.exampleModule = {
    "attach": function (context, settings) {
      // ['mm.foundation', 'ui.layout', 'experdServices', 'experdControllers', 'experdWorkflow', 'ngRoute']
      /**
       * @ngdoc overview
       * @name ExperD
       * @description
       * #ExperD documentation.
       * This is the documentation for the front-end part of ExperD.  
       * It is build using angularjs and loaded from the Drupal backend.  
       * From Drupal, the initialization data is set using _Drupal.settings_.  
       * This includes an array of all of the module dependencies for this 
       * angular app in _"Drupal.settings.experd.angularModuleList"_.  
       * **Plugins** can add their own angular modules to this list.  
       * It uses the following external libraries:
       * - **{@link http://www.jsplumb.org/doc/home.html jsPlumb}**  
       * jsPlumb provides a means for a developer to visually connect elements.  
       * - **krytWorkflow**  
       *  A jquery wrapper around jsPlumb optimized for ExperD workflows
       * - **{@link http://pineconellc.github.io/angular-foundation/ angular-foundation}**  
       * Angular implementation of directives for the foundation framework 
       * (uses foundation css).
       * - **{@link http://angular-ui.github.io/ui-layout/ ui-layout}**  
       * For the splittable panes.
       * - **{@link http://fortawesome.github.io/Font-Awesome/icons/ font awesome}**  
       * For the nice icons.
       */

      // load angular module list from Drupal.settings.
      var moduleList = Drupal.settings.experd.angularModuleList;
      angular.module('experdapp', moduleList)
        .config(["$provide", function ($provide) {
            if (window.Drupal) {
              $provide.constant('appRoot', Drupal.settings.basePath + Drupal.settings.experd.modulePath + '/app/');
              $provide.constant('serverUrl', window.location.protocol + "//" + window.location.host + Drupal.settings.basePath);
            }
          }])
        .config(['$locationProvider', function ($locationProvider) {
            $locationProvider.hashPrefix('');
          }])
        .config(['$routeProvider', 'appRoot',
          function ($routeProvider, appRoot) {
            $routeProvider.
              when('/', {
                templateUrl: appRoot + 'workflow/main-view-fnd.html',
                controller: 'ExperDController',
                controllerAs: 'experdCtrl'
              }).
              otherwise({
                redirectTo: '/'
              });
          }
        ]);
    }
  };
})();