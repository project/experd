(function () {
  "use strict"; //ECMA5 strict modus
  /**
   * 
   * @ngdoc overview
   * @name experdControllers
   * @description 
   * ##ExperD controller
   * The controller is linked to the main view using the controllerAs syntax as **'experdCtrl'**.
   */

  var experdControllers = angular.module('experdControllers', []);
  /**
   * 
   * @ngdoc controller
   * @name experdControllers.controller:ExperDController
   * @requires experdServices.ExperDAPI
   * @requires experdServices.ExperDOperationService
   * @requires $scope
   * @listens experdWorkflow.directive:krytWorkflow#krytWorkflow.moveFigure
   * @description 
   * Main experd controller linking the workflow to the Drupal webservices.    
   * It handles the workflow events emitted by the krytWorkflow directive.
   */

  experdControllers.controller('ExperDController', ['ExperDAPI', 'ExperDOperationService', 'appRoot', '$scope', '$location', '$modal', function (ExperDAPI, ExperDOperationService, appRoot, $scope, $location, $modal) {
      var self = this;

      this.sizes = {
        operationsToolbar: '110px',
        propertiesEditor: '25%'
      };
      
      /**
       * @ngdoc property
       * @name experdControllers.controller:ExperDController.expdList
       * @propertyOf experdControllers.controller:ExperDController
       * @returns {array} List of all ExperD nodes for this user
       */
      this.expdList = [];

      /**
       * @ngdoc property
       * @name experdControllers.controller:ExperDController.currentExpdGroups
       * @propertyOf experdControllers.controller:ExperDController
       * @returns {array} List of groups for current ExperD and current user
       */
      this.currentExpdGroups = ExperDOperationService.getCurrentExpdGroups;

      this.currentOperations = ExperDOperationService.getCurrentOperations; // List of all operations for current ExperD

      this.config = ExperDOperationService.config;

      //this.displayList = ExperDOperationService.currentOperationStates; //operation states displayed and watched by workflow

      /**
       * @ngdoc property
       * @name experdControllers.controller:ExperDController.selectedOperationUuid
       * @propertyOf experdControllers.controller:ExperDController
       * @returns {string} uuid of the selected operation selected in the workflow
       */
      this.selectedOperationUuid = "";

      this.Init = function () {
        var experdID = $location.search().experdID;
        if (experdID) {
          self.loadExperd(experdID);
        }
      }

      /**
       * @ngdoc method
       * @name experdControllers.controller:ExperDController.deleteSelectedOperation
       * @methodOf experdControllers.controller:ExperDController
       * @function
       * @description
       * Delete the selected operation
       */
      this.deleteSelectedOperation = function () {
        var r = confirm("Do you really want to delete the selected operation?");
        if (r === true) {
          $scope.deleteFigure(self.selectedOperationUuid);
        }
      };

      /**
       * @ngdoc method
       * @name experdControllers.controller:ExperDController.isOperationSelected
       * @methodOf experdControllers.controller:ExperDController
       * @function
       * @description
       * Check for a selected operation
       * @returns {boolean} TRUE if an operation is selected in the workflow
       */
      this.isOperationSelected = function () {
        if (!ExperDOperationService.getStateByUuid(self.selectedOperationUuid)) {
          // selectedOperationUuid does not exist, probably deleted by another client
          self.selectedOperationUuid = '';
        }
        return (self.selectedOperationUuid.length > 0);
      };

      //$on displaylist.update event send from ExperDOperationService
//      $scope.$on('displaylist.update', function (event) {
//        self.displayList = ExperDOperationService.currentOperationStates;
//      });

      $scope.$on('krytWorkflow.figureClick', function (event, uuid) {
        self.selectedOperationUuid = uuid;
      });

      $scope.$on('krytWorkflow.addFigure', function (event, nid, uuid, byGUI) {
        if (byGUI) {
          ExperDOperationService.newOperationState(nid, uuid);
        }
      });

      $scope.$on('krytWorkflow.moveFigure', function (event, figureUuid, x, y, byGUI) {
        if (byGUI) {
          var pluginData = {
            'action': 'move',
            'location': {
              'x': x,
              'y': y
            }
          };
          // send new position
          ExperDOperationService.updateOperationStatePluginData(figureUuid, 'experd', pluginData)
        }
      });

      $scope.$on('krytWorkflow.connect', function (event, outFigureUuid, outPortId, inFigureUuid, inPortId, byGUI) {
        if (byGUI) {
          // get existing connections
          var data = ExperDOperationService.getOperationStatePluginData(outFigureUuid, 'experd');
          var connectionsOut = angular.copy(data.connectionsOut);
          var newConnection = {};
          newConnection.sourcePortId = outPortId;
          newConnection.targetOperationJsNid = inFigureUuid;
          newConnection.targetPortId = inPortId;
          //add new connection
          connectionsOut.push(newConnection);
          var pluginData = {
            'action': 'connect',
            'connectionsOut': connectionsOut
          };
          // send updated connections
          ExperDOperationService.updateOperationStatePluginData(outFigureUuid, 'experd', pluginData)
        }
      });

      $scope.$on('krytWorkflow.disconnect', function (event, outFigureUuid, outPortId, inFigureUuid, inPortId, byGUI) {
        if (byGUI) {
          var data = ExperDOperationService.getOperationStatePluginData(outFigureUuid, 'experd');
          var connectionsOut = data.connectionsOut;
          for (var i = connectionsOut.length - 1; i >= 0; i--) {
            var connection = connectionsOut[i];
            if (connection.sourcePortId == outPortId && connection.targetOperationJsNid == inFigureUuid && connection.targetPortId == inPortId) {
              connectionsOut.splice(i, 1);
            }
          }
          var pluginData = {
            'action': 'disconnect',
            'connectionsOut': connectionsOut
          };
          ExperDOperationService.updateOperationStatePluginData(outFigureUuid, 'experd', pluginData)
        }
      });

      $scope.$on('krytWorkflow.removeFigure', function (event, figureUuid, byGUI) {
        if (byGUI) {
          var state = angular.copy(ExperDOperationService.getStateByUuid(figureUuid));
          if (self.selectedOperationUuid === figureUuid) {
            self.selectedOperationUuid = "";
          }
          if (state && state.plugins) {
            state.plugins = {}; // send changes only
            state.databaseAction = 'delete';
            state.plugins.experd = {
              'action': 'delete'
            };
            self.updateOperationState(state);
          }
        }
      });

      // called from ng-init in main-view
      /**
       * @ngdoc method
       * @name experdControllers.controller:ExperDController.listExperds
       * @methodOf experdControllers.controller:ExperDController
       * @function
       * @description
       * Populate the list of available ExperD's.
       */
      this.listExperds = function () {
        ExperDAPI.index().then(function (response) {
          self.expdList = response.data;
          self.Init();
        });
      };

      /**
       * @ngdoc method
       * @name experdControllers.controller:ExperDController.loadExperd
       * @methodOf experdControllers.controller:ExperDController
       * @function
       * @param {int} nid ExperD's node id.
       * @description
       * Load the selected ExperD  
       */
      this.loadExperd = function (nid) {
        ExperDOperationService.loadExperd(nid);
        //self.getGroups(nid);
        //self.loadOperations(nid);
        //ExperDOperationService.getSnapshots(nid);
      };

      /**
       * @ngdoc method
       * @name experdControllers.controller:ExperDController.isExperdSelected
       * @methodOf experdControllers.controller:ExperDController
       * @function
       * @returns {boolean} TRUE if ExperD is selected
       */
      this.isExperdSelected = function () {
        return ExperDOperationService.xpnid > 0;
      };

      /**
       * @ngdoc method
       * @name experdControllers.controller:ExperDController.getExperdTitle
       * @methodOf experdControllers.controller:ExperDController
       * @function
       * @returns {string} Title of selected ExperD.
       */
      this.getExperdTitle = function () {
        return ExperDOperationService.currentExpd.title;
      };

      /**
       * @ngdoc method
       * @name experdControllers.controller:ExperDController.isSaving
       * @methodOf experdControllers.controller:ExperDController
       * @function
       * @returns {boolean} TRUE if workflow data is being saved to server.
       */
      this.isSaving = function () {
        return ExperDOperationService.status.saving;
      };

      /**
       * @ngdoc method
       * @name experdControllers.controller:ExperDController.isLocked
       * @methodOf experdControllers.controller:ExperDController
       * @function
       * @returns {boolean} TRUE if workflow is readonly.
       */
      this.isLocked = function () {
        return ExperDOperationService.status.readonly;
      };

      /**
       * @ngdoc method
       * @name experdControllers.controller:ExperDController.isUpdating
       * @methodOf experdControllers.controller:ExperDController
       * @function
       * @returns {boolean} TRUE if workflowdata is being retrieved from server.
       */
      this.isUpdating = function () {
        return ExperDOperationService.status.updating;
      };

      /**
       * @ngdoc method
       * @name experdControllers.controller:ExperDController.isErrorState
       * @methodOf experdControllers.controller:ExperDController
       * @function
       * @returns {boolean} TRUE if synchronization error has occurred.
       */
      this.isErrorState = function () {
        return ExperDOperationService.status.error;
      };

      /**
       * @ngdoc method
       * @name experdControllers.controller:ExperDController.getLastSavedSid
       * @methodOf experdControllers.controller:ExperDController
       * @function
       * @returns {int} sid of last saved operationstate.
       */
      this.getLastSavedSid = function () {
        return ExperDOperationService.status.last_saved_sid;
      };

      /**
       * @ngdoc method
       * @name experdControllers.controller:ExperDController.getLastReceivedSid
       * @methodOf experdControllers.controller:ExperDController
       * @function
       * @returns {int} sid of last received operationstate.
       */
      this.getLastReceivedSid = function () {
        return ExperDOperationService.status.last_received_sid;
      };

      /**
       * @ngdoc method
       * @name experdControllers.controller:ExperDController.updateOperationState
       * @methodOf experdControllers.controller:ExperDController
       * @function
       * @param {object} operationState JSON formatted operation state data
       * @description
       * Send new or updated operation state data to the server.
       */
      this.updateOperationState = function (operationState) {
        ExperDOperationService.updateOperationState(operationState);
      };

      /**
       * @ngdoc method
       * @name experdControllers.controller:ExperDController.saveSnapshot
       * @methodOf experdControllers.controller:ExperDController
       * @function
       * @description
       * Save a snapshot of the current ExperD workflow.
       */
      this.saveSnapshot = function () {
        var name = window.prompt("Enter snapshot name", "Snapshot");
        if (name !== null) {
          ExperDOperationService.saveSnapshot(name);
        }
      };

      /**
       * @ngdoc method
       * @name experdControllers.controller:ExperDController.snapshots
       * @methodOf experdControllers.controller:ExperDController
       * @function
       * @returns {array} Array of snapshots
       * @description
       * Return a list of all snapshots for the current ExperD.
       */
      this.snapshots = function () {
        return ExperDOperationService.snapshots;
      };

      /**
       * @ngdoc method
       * @name experdControllers.controller:ExperDController.loadSnapshot
       * @methodOf experdControllers.controller:ExperDController
       * @function
       * @param {int} snap_id The selected snapshot id
       * @param {bool} autosave If true: save current workflow schema as snapshot
       * @description
       * Load the selected snapshot with id snap_id.
       */
      this.loadSnapshot = function (snap_id, autosave) {
        $scope.clearWorkflow();
        ExperDOperationService.loadSnapshot(snap_id, autosave);
      };

      /**
       * @ngdoc method
       * @name experdControllers.controller:ExperDController.selectSnapshot
       * @methodOf experdControllers.controller:ExperDController
       * @function
       * @description
       * Opens a modal dialog to select a snapshot and set it as the current schema.
       */
      this.selectSnapshot = function () {
        var modalInstance = $modal.open({
          templateUrl: appRoot + '/workflow/select-snapshot.html',
          resolve: {
            snapshots: function () {
              return self.snapshots();
            }
          },
          controller: function ($scope, $modalInstance, snapshots) {
            $scope.snapshots = snapshots;
            $scope.ok = function (snap_id, autosave) {
              $modalInstance.close({'snap_id': snap_id, 'autosave': autosave});
            };

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };

          }
        });

        modalInstance.result.then(function (result) {
          self.loadSnapshot(result.snap_id, result.autosave);
        }, function () {
          //$log.info('Modal dismissed at: ' + new Date());
        });
      };

      /**
       * @ngdoc method
       * @name experdControllers.controller:ExperDController.ReloadWorkflow
       * @methodOf experdControllers.controller:ExperDController
       * @function
       * @description
       * Clear the workflow and reload workflow from server.
       */
      this.ReloadWorkflow = function () {
        $scope.clearWorkflow();
        ExperDOperationService.Reload();
      };

      this.operationTabs = function () {
        var tabs = ExperDOperationService.getCurrentOperationsByCategory();
        tabs['Custom'] = self.getCustomOperations();
        return tabs;
      };

      // Temporary hack to create a basic dummy operation 
      var templateOperation = {
        "-1": {
          "name": "Change name",
          "nid": "-1",
          "category": "13",
          "classification": {
            "name": "Custom",
            "tid": "-1"
          },
          "imageUrl": appRoot + "/assets/img/questionmark.jpg",
          "labbuddy_method": null,
          "description": "You can change this description",
          "inputPorts": [
            {
              "name": "In",
              "nid": "-1",
              "port_type": "1"
            }
          ],
          "outputPorts": [
            {
              "name": "Out",
              "nid": "-2",
              "port_type": "2"
            }
          ]
        }
      };

      this.getCustomOperations = function () {
        return templateOperation;
      };

      this.getAllOperations = function () {
        var ops = ExperDOperationService.getCurrentOperations();
        return jQuery.extend(ops, templateOperation);
      };
      
    }]);
})();