(function () {
  var experdServices = angular.module('experdServices', []);
  /**
   * @ngdoc overview
   * @name experdServices
   * @description
   * ##JSON
   * An operation state described by this JSON format:
   * <pre>
   * [
   *  {
   *    "sid": 12,  // int unique id of the operation state
   *    "operationJsNid": "1234567890ABCDEF1234567890ABCDEF", //char(32)
   *    "operationDbNid": 69, //int
   *    "databaseAction": 'add||delete', // only when adding or deleting operation
   *    "plugins": {
   *      "experd": { // experd is also handled as a plugin
   *        "action": "add||delete||move||connect||disconnet",  //char(10)
   *        "location": {     //char(45)
   *          "x": 200,
   *          "y": 180
   *        },
   *        "dimensions": {   //char(45)
   *          "width": 100,
   *          "height": 80
   *        },
   *        "connectionsOut": [ //char(1024)
   *          {
   *            "sourcePortId": "string",
   *            "targetOperationJsNid": "string",
   *            "targetPortId": "string"
   *          }
   *        ]
   *      },
   *      "myplugin": {
   *        "myplugindata":{}
   *      }
   *    }
   *  }
   * ];
   * </pre>
   * ## Send changes, receive states
   * If data for a changed state is posted, only the data for the plugin that made
   * the change is added to the state data and sent to the server.
   * The state data returned from the server always contains the latest data from all plugins.
   * ##Plugins
   * A plugin can add its own data to the "plugins" object, using the pluginname as key.  
   * A "plugin.updated.{pluginname}" event is emitted when new data is available for the plugin.  
   * The plugin can listen for this event and receive the data.  
   * 
   */
  /**
   * 
   * @ngdoc service
   * @name experdServices.ExperDAPI
   * @requires $http
   * @description 
   * Implements API for Drupal experd webservices
   */
  experdServices.factory('ExperDAPI', ['$http', 'serverUrl', function ($http, serverUrl) {
      var apiURL = serverUrl + '?q=experd_api/experd';
      var file_apiURL = serverUrl + '?q=experd_api/file';
      // add X-CSRF-Token to the header, so the POST calls are authenticated
      if (window.Drupal && window.Drupal.settings.experd.token) {
        $http.defaults.headers.post['X-CSRF-Token'] = window.Drupal.settings.experd.token;
        // add token for delete (no headers.delete so use common ?).
        $http.defaults.headers.common['X-CSRF-Token'] = window.Drupal.settings.experd.token;
      }

      return {
        /**
         * @ngdoc method
         * @name experdServices.ExperDAPI#get
         * @function
         * @methodOf experdServices.ExperDAPI
         * @param {int} nid Node id for ExperD node
         * @description 
         * Webservice call:
         * - GET http://domain/endpoint/experd/{nid}.json  
         *   return experd with id {nid}
         */
        get: function (nid) {
          return $http.get(apiURL + '/' + nid + '.json', {cache: 'true'});
        },
        /**
         * @ngdoc method
         * @name experdServices.ExperDAPI#index
         * @function
         * @methodOf experdServices.ExperDAPI
         * @description
         * Webservice call:
         * - GET http://domain/endpoint/experd.json  
         *   return array of available experd nodes for current user
         */
        index: function () {
          return $http.get(apiURL + '.json', {cache: 'true'});
        },
        /**
         * @ngdoc method
         * @name experdServices.ExperDAPI#operations
         * @function
         * @methodOf experdServices.ExperDAPI
         * @param {int} nid Node id for ExperD node
         * @description
         * Webservice call:
         * - GET http://domain/endpoint/experd/{nid}/operations.json  
         *   return array of operations for experd with id {nid}
         */
        operations: function (nid) {
          return $http.get(apiURL + '/' + nid + '/operations.json', {cache: 'true'});
        },
        /**
         * @ngdoc method
         * @name experdServices.ExperDAPI#groups
         * @function
         * @methodOf experdServices.ExperDAPI
         * @param {int} nid Node id for ExperD node
         * @description
         * Webservice call:
         * - GET http://domain/endpoint/experd/{nid}/groups.json  
         *   return array of groups for experd with id {nid} for current user.
         */
        groups: function (nid) {
          return $http.get(apiURL + '/' + nid + '/groups.json', {cache: 'true'});
        },
        /**
         * @ngdoc method
         * @name experdServices.ExperDAPI#getStatesFromSid
         * @function
         * @methodOf experdServices.ExperDAPI
         * @param {int} nid Node id for ExperD node
         * @param {int=} [sid=0] start with savedoperation with id {sid} 
         * @param {int=} [gid=first valid group found for user] return savedoperations for group with id {gid}
         * @description
         * Webservice call:
         * - GET http://domain/endpoint/experd/{nid}/getStatesFromSid/{sid}/{gid}.json  
         *   return savedoperation states for experd with id {nid}
         */
        getStatesFromSid: function (nid, sid, gid) {
          return $http.get(apiURL + '/' + nid + '/getStatesFromSid' + (sid ? '/' + sid : '/0') + (gid ? '/' + gid : '') + '.json', {cache: false});
        },
        /**
         * @ngdoc method
         * @name experdServices.ExperDAPI#addOperationState
         * @function
         * @methodOf experdServices.ExperDAPI
         * @param {int} nid Node id for ExperD node
         * @param {array} data array of changes in states to store in de database
         * @param {int=} [sid=0] start with savedoperation with id {sid}
         * @param {int=} [gid=first valid group found for user] return savedoperations for group with id {gid}
         * @description
         * Webservice call:
         * - POST http://domain/endpoint/experd/{nid}/addOperationState/{sid}/{gid}.json  
         *   post array of savedoperation states to store in de database for experd 
         *   with id {nid} and group id {gid} (optional).  
         *   return operation states from sid (default = 0)  
         *   data format: see "dummydata.js"
         */
        addOperationState: function (nid, data, sid, gid) {
          return $http.post(apiURL + '/' + nid + '/addOperationState' + (sid ? '/' + sid : '/0') + (gid ? '/' + gid : '') + '.json', data, {cache: false});
        },
        /**
         * @ngdoc method
         * @name experdServices.ExperDAPI#saveSnapshot
         * @function
         * @methodOf experdServices.ExperDAPI
         * @param {int} nid Node id for ExperD node
         * @param {array} data Snapshot data (data[0] = name of snapshot).
         * @param {int=} [gid=first valid group found for user] Save snapshot for group with id {gid}
         * @returns {int} id of saved snapshot
         * @description
         * Webservice call:
         * - POST http://domain/endpoint/experd/{nid}/saveSnapshot/{gid}.json  
         *   Save a snapshot of the current experd workflow schema. 
         *   with id {nid} and group id {gid} (optional).  
         *   return snap_id 
         */
        saveSnapshot: function (nid, data, gid) {
          return $http.post(apiURL + '/' + nid + '/saveSnapshot' + (gid ? '/' + gid : '') + '.json', data, {cache: false});
        },
        /**
         * @ngdoc method
         * @name experdServices.ExperDAPI#getSnapshots
         * @function
         * @methodOf experdServices.ExperDAPI
         * @param {int} nid Node id for ExperD node
         * @param {int=} [gid=first valid group found for user] Save snapshot for group with id {gid}
         * @description
         * Webservice call:
         * - POST http://domain/endpoint/experd/{nid}/getSchemaSnapshots/{gid}.json  
         *   Retrieve a list of all snapshots for ExperD 
         *   with id {nid} and group id {gid} (optional).  
         */
        getSnapshots: function (nid, gid) {
          return $http.get(apiURL + '/' + nid + '/getSchemaSnapshots' + (gid ? '/' + gid : '') + '.json', {cache: false});
        },
        /**
         * @ngdoc method
         * @name experdServices.ExperDAPI#restoreSnapshot
         * @function
         * @methodOf experdServices.ExperDAPI
         * @param {int} nid Node id for ExperD node
         * @param {int} snap_id Snapshot id
         * @param {boolean} autosave If true: save a snapshot of current schema as backup
         * @param {int=} [gid=first valid group found for user] Save snapshot for group with id {gid}
         * @description
         * Webservice call:
         * - POST http://domain/endpoint/experd/{nid}/restoreSchemaSnapshotById/{snap_id}/{autosave}/{gid}.json  
         *   Retrieve a snapshot for ExperD 
         *   with id {nid} and group id {gid} (optional).  
         *   Save as current schema
         */
        restoreSnapshot: function (nid, snap_id, autosave, gid) {
          return $http.get(apiURL + '/' + nid + '/restoreSchemaSnapshotById' + '/' + snap_id + (autosave ? '/1' : '/0') + (gid ? '/' + gid : '') + '.json', {cache: false});
        },
        /**
         * @ngdoc method
         * @name experdServices.ExperDAPI#uploadFile
         * @function
         * @methodOf experdServices.ExperDAPI
         * @param {array} data File info (base-64)
         */
        uploadFile: function (data) {
          return $http.post(file_apiURL + '.json', data);
        },
        /**
         * @ngdoc method
         * @name experdServices.ExperDAPI#uploadRawFile
         * @function
         * @methodOf experdServices.ExperDAPI
         * @param {array} data File data
         */
        uploadRawFile: function (data) {
          return $http.post(file_apiURL + '/create_raw', data);
        },
        /**
         * @ngdoc method
         * @name experdServices.ExperDAPI#retrieveFile
         * @function
         * @methodOf experdServices.ExperDAPI
         * @param {int} fid Drupal file id
         */
        retrieveFile: function (fid) {
          return $http.get(file_apiURL + '/' + fid + '.json', {params: {file_contents: 0}});
        },
        /**
         * @ngdoc method
         * @name experdServices.ExperDAPI#deleteFile
         * @function
         * @methodOf experdServices.ExperDAPI
         * @param {int} fid Drupal file id
         */
        deleteFile: function (fid) {
          return $http.delete(file_apiURL + '/' + fid + '.json');
        }
      };
    }
  ]);

  /**
   * @ngdoc service
   * @name experdServices.ExperDOperationService
   * @requires $timeout
   * @requires $rootScope
   * @description 
   * Manage get/set/update/sync of operationstates for workflow
   * 
   * To allow synchronization of workflows form different clients on differen computers,
   * the service maintains a number of state buffers:
   *
   * - **currentOperationStates**:   
   * This is the list of all active operation states (received from the server)
   * When new operationstates are received, they are added to or replaced 
   * in the currentOperationStates. This list is maintained in the 
   * {@link experdServices.ExperDOperationService#methods_updatecurrentoperationstates updateCurrentOperationStates} function.
   * 
   * - **addedOperationStates** (edit buffer):   
   * When a change is made in the workflow editor, the changed state is 
   * added to addedOperationStates.
   * 
   * - **sentOperationStates** (sent buffer):   
   * When the addeOperationstates are sent, they are moved to the sentOperationStates
   * and the addedOperationStates are cleared.
   * 
   * - **savedOperationStates** (receive buffer):   
   * This is the last received set of operationstates from the server (added, updated or deleted).
   * When new operationstates are received, the changes are added to, updated in, or deleted from 
   * the currentOperationStates and the sentOperationStates are cleared.
   * 
   */
  experdServices.service('ExperDOperationService', ['ExperDAPI', '$timeout', '$rootScope', '$window', function (ExperDAPI, $timeout, $rootScope, $window) {
      var self = this;
      var timeoutPromise;

      /**
       * @ngdoc property
       * @name experdServices.ExperDOperationService#xpnid
       * @propertyOf experdServices.ExperDOperationService
       * @returns {int}  nid for current ExperD node
       */
      this.xpnid = null;

      /**
       * @ngdoc property
       * @name experdServices.ExperDOperationService#currentExpd
       * @propertyOf experdServices.ExperDOperationService
       * @returns {object} currently loaded ExperD data
       */
      this.currentExpd = {};

      /**
       * @ngdoc property
       * @name experdServices.ExperDOperationService#currentOperations
       * @propertyOf experdServices.ExperDOperationService
       * @returns {object} list of operation for current ExperD
       */
      this.currentOperations = {};

      this.currentExpdGroups = [];
      /**
       * @ngdoc property
       * @name experdServices.ExperDOperationService#operationCategories
       * @propertyOf experdServices.ExperDOperationService
       * @returns {object} operations by category to fill the category tabs
       */
      this.operationCategories = {};

      this.currentOperationStates = {}; //list of all current operationstates
      this.savedOperationStates = []; //array of last set of operationstates received from server
      this.addedOperationStatesQ = []; //list of unsaved operation states
      // changed in workflow, but not sent to server
      this.sentOperationStates = [];  //list of operationstates sent to server 
      //but not returned in currentOperationStates

      var oldOperationStates = {};

      this.snapshots = {};

      /**
       * @ngdoc property
       * @name experdServices.ExperDOperationService#config
       * @propertyOf experdServices.ExperDOperationService
       * @returns {object} configuration settings for workflow
       */
      this.config = {
        autosave: false, // save every change
        autoUpdate: true,
        pollingInterval: 5000
      };

      /**
       * @ngdoc property
       * @name experdServices.ExperDOperationService#status
       * @propertyOf experdServices.ExperDOperationService
       * @returns {object} status information
       */
      this.status = {
        updating: false,
        saving: false,
        readonly: false,
        last_saved_sid: 0,
        last_received_sid: -1
      };

      /**
       * @ngdoc method
       * @name experdServices.ExperDOperationService#loadExperd
       * @function
       * @methodOf experdServices.ExperDOperationService
       * @param {int} nid Node id for ExperD
       * @description
       * Load data for ExperD node with id nid
       * 
       */
      this.loadExperd = function (nid) {
        ExperDAPI.get(nid).success(function (data) {
          self.currentExpd = data;
          self.xpnid = nid;
          self.loadExperdGroups(nid);
          self.loadOperations(nid);
          self.getSnapshots(nid);
        });
      };

      /**
       * @ngdoc method
       * @name experdServices.ExperDOperationService#loadExperdGroups
       * @methodOf experdServices.ExperDOperationService
       * @function
       * @param {int} nid ExperD's node id.
       * @description
       * Load group information for ExperD with node id nid and current user.
       */
      this.loadExperdGroups = function (nid) {
        ExperDAPI.groups(nid).success(function (data) {
          self.currentExpdGroups = data;
        });
      };

      /**
       * @ngdoc method
       * @name experdServices.ExperDOperationService#getCurrentExpdGroups
       * @methodOf experdServices.ExperDOperationService
       * @function
       * @description
       * Get group information for current ExperD and current user.
       */
      this.getCurrentExpdGroups = function () {
        return self.currentExpdGroups;
      };

      /**
       * @ngdoc function
       * @name experdServices.ExperDOperationService#getCurrentOperations
       * @function
       * @methodOf experdServices.ExperDOperationService
       * @return {object} currentOperations List of operation for selected ExperD
       * @description
       * Get a list of all operation for the selected ExperD.  
       * This list is used by krytWorkflow.
       */
      this.getCurrentOperations = function () {
        return self.currentOperations;
      };

      /**
       * @ngdoc function
       * @name experdServices.ExperDOperationService#getCurrentOperationsByCategory
       * @function
       * @methodOf experdServices.ExperDOperationService
       * @return {object} operationCategories List of operation for selected ExperD organized by Category
       * @description
       * Get a list of all operation for the selected ExperD organized by category.  
       * This list is used by the krytWorkflow tool bar to display the operations in tabs (one tab per category).
       */
      this.getCurrentOperationsByCategory = function () {
        return self.operationCategories;
      };

      /**
       * @ngdoc function
       * @name experdServices.ExperDOperationService#loadOperations
       * @function
       * @methodOf experdServices.ExperDOperationService
       * @param {int} nid Node id for ExperD
       * @description
       * Load operations for ExperD with node id nid
       * Calls createCategories and getSavedStates to bootstrap the 
       * display of the workflow
       */
      this.loadOperations = function (nid) {
        ExperDAPI.operations(nid).success(function (data) {
          self.currentOperations = data;
          createCategories();
          self.getSavedStates(nid);
        });
      };

      function createCategories() {
        var cats = {};
        var index = 0;
        for (var op in self.currentOperations) {
          var operation = self.currentOperations[op];
          var key = operation.classification.name;
          if (cats[key]) {
            operation.category = index;
            cats[key].push(operation);
          } else {
            index++;
            operation.category = index;
            cats[key] = [operation];
          }
        }
        self.operationCategories = cats;
      }

      /**
       * @ngdoc method
       * @name experdServices.ExperDOperationService#getStateByUuid
       * @function
       * @methodOf experdServices.ExperDOperationService
       * @param {string} uuid Unique id for operationstate
       * @return {object} JSON formatted state data
       * @description
       * Get state data for operation state with uuid
       */
      this.getStateByUuid = function (uuid) {
        return self.currentOperationStates[uuid];
      };

      /**
       * @ngdoc event
       * @name experdServices.ExperDOperationService#displaylist(dot)updated
       * @eventOf experdServices.ExperDOperationService
       * @eventType broadcast on $rootScope
       * @param {object} currentOperationStates List of all current operation states
       * @param {object} oldStates List of all previous operation states
       * @param {array} updatedUuidList List of uuid's of all updated operation states
       * @param {array} deletedUuidList List of uuid's of all deleted operation states
       * @description
       * Event name: **'displaylist.updated'**
       * *Names with '.' dont work properly in ngdocs*  
       * Notification that some operation states have been updated or deleted.  
       * Plugins (like the experd workflow)can listen to this event to react on the changes
       */

      /**
       * @ngdoc method
       * @name experdServices.ExperDOperationService#updateCurrentOperationStates
       * @function
       * @methodOf experdServices.ExperDOperationService
       * @description
       * Update the list of currentOperationStates.  
       * This function is responsible for maintaining the different state buffers,
       * and keeping the currentOperationStates up to date.  
       * The function is called when an operation state is posted to the server, 
       * or when new states are received from the server.  
       * This function emits the {@link experdServices.ExperDOperationService#events_displaylistupdated 'displaylist.updated'} event.
       */
      var updateCurrentOperationStates = function () {
        // process new received operation states
        // update currentOperationStates with new savedOperationStates
        // 

        if (self.savedOperationStates.length === 0
                && self.sentOperationStates.length === 0
                && self.addedOperationStatesQ.length === 0) {
          // nothing to do
          return;
        }
        var activeStates = self.savedOperationStates;
        var deletedUuidList = [];
        var updatedUuidList = [];
        // handle last received states from server
        for (var i = activeStates.length - 1; i >= 0; i--) {
          var uuid = activeStates[i].operationJsNid;
          if (activeStates[i].databaseAction === 'delete') {
            // delete 'deleted' states from currentOperationStates
            deletedUuidList.push(uuid);
            delete self.currentOperationStates[uuid];
          } else {
            // add/update states
            if (updatedUuidList.indexOf(uuid) === -1) {
              updatedUuidList.push(uuid);
            }
            self.currentOperationStates[uuid] = angular.copy(activeStates[i]);
          }
        }
        // copy currentOperationStates to displaylist
        var displayList = self.currentOperationStates;

        function stateIterator(stateData, index, operationStates) {
          var uuid = stateData.operationJsNid;
          if (updatedUuidList.indexOf(uuid) === -1) {
            updatedUuidList.push(uuid);
          }
          var state = angular.copy(displayList[uuid]);
          if (!angular.isDefined(state)) { // New state
            state = stateData;
          } else { // update plugin data
            // extend does not do a recursive extend
            // just one level for each plugin should be enough
            angular.forEach(stateData.plugins, function (pluginData, pluginName) {
              jQuery.extend(state.plugins[pluginName], pluginData);
            });
          }
          displayList[uuid] = state;
        }

        //merge changed properies from sent changes
        if (self.sentOperationStates.length > 0) {
          angular.forEach(self.sentOperationStates, stateIterator);
        }
        //merge changed properies from unsaved changes
        if (self.addedOperationStatesQ.length > 0) {
          angular.forEach(self.addedOperationStatesQ, stateIterator);
        }

        // send oldStates, so event handlers can look for changes
        var oldStates = oldOperationStates; //angular.copy(self.currentOperationStates);
        //broadcast update notification event
        $rootScope.$broadcast('displaylist.updated', displayList, oldStates, updatedUuidList, deletedUuidList);
        // save as old states 
        oldOperationStates = angular.copy(displayList);
      };

      /**
       * @ngdoc method
       * @name experdServices.ExperDOperationService#newOperationState
       * @function
       * @methodOf experdServices.ExperDOperationService
       * @param {int} nid Node id of the added experd operation
       * @param {string} uuid Unique id for operationstate
       * @description
       * Create state data for new operation state with uuid and send to the server.
       */
      this.newOperationState = function (nid, uuid) {
        var newOperation = {
          'operationJsNid': uuid,
          'operationDbNid': nid,
          'databaseAction': 'add',
          'plugins': {
            'experd': {
              'action': 'add',
              'location': {//default location
                'x': 0,
                'y': 0
              },
              'dimensions': {// default dimensions
                'width': 100,
                'height': 80
              },
              'connectionsOut': []
            }
          }
        };
        self.updateOperationState(newOperation);
      };

      /**
       * @ngdoc method
       * @name experdServices.ExperDOperationService#updateOperationState
       * @function
       * @methodOf experdServices.ExperDOperationService
       * @param {object} operationState JSON formatted operation state data
       * @description
       * Send new or updated operation state data to the server.
       */
      this.updateOperationState = function (operationState) {
        var saveCopy = angular.copy(operationState);
        var uuid = saveCopy.operationJsNid;
        delete (saveCopy.sid);
        self.addedOperationStatesQ.push(saveCopy);
        //self.addedOperationStates[uuid] = jQuery.extend(true, self.addedOperationStates[uuid], saveCopy); // merge multiple updates
        updateCurrentOperationStates();
        if (self.config.autosave) { // send immedialtely (default = false: wait for next polling cycle)
          self.syncStatesWithDB(self.xpnid, self.status.last_received_sid + 1);
        }
      };

      /**
       * @ngdoc method
       * @name experdServices.ExperDOperationService#updateOperationStatePluginData
       * @function
       * @methodOf experdServices.ExperDOperationService
       * @param {string} uuid Unique id for operationstate
       * @param {string} pluginname Name of plugin the data belongs to
       * param {object} data JSON formatted state data
       * @description
       * Create state data for new operation state with uuid and send to the server.
       */
      this.updateOperationStatePluginData = function (uuid, pluginname, data) {
        var state = angular.copy(self.currentOperationStates[uuid]);
        if (state) {
          delete (state.sid);
          state.databaseAction = 'update';
          state.plugins = {};
          state.plugins[pluginname] = data;
          self.addedOperationStatesQ.push(state);
          //self.addedOperationStates[uuid] = jQuery.extend(true, self.addedOperationStates[uuid], state); // merge multiple updates
          updateCurrentOperationStates();
          if (self.config.autosave) { // send immedialtely (default = false: wait for next polling cycle)
            self.syncStatesWithDB(self.xpnid, self.status.last_received_sid + 1);
          }
        }
      };

      /**
       * @ngdoc method
       * @name experdServices.ExperDOperationService#getOperationStatePluginData
       * @function
       * @methodOf experdServices.ExperDOperationService
       * @param {string} uuid Unique id for operationstate
       * @param {string} pluginname name of plugin to get data for
       * @return {object} JSON formatted state data
       * @description
       * Get state data for plugin with pluginname from operation state with uuid.
       */
      this.getOperationStatePluginData = function (uuid, pluginname) {
        return self.currentOperationStates[uuid].plugins[pluginname];
      };

      /**
       * @ngdoc method
       * @name experdServices.ExperDOperationService#Reload
       * @function
       * @methodOf experdServices.ExperDOperationService
       * @description
       * Reload current ExperD.  
       * Clear all buffers and reload all operation states from sid = 0.
       */
      this.Reload = function () {
        self.clearBuffers();
        this.getSavedStates(self.xpnid, 0);
      };

      /**
       * @ngdoc method
       * @name experdServices.ExperDOperationService#clearBuffers
       * @function
       * @methodOf experdServices.ExperDOperationService
       * @description
       * Clear all buffers and cancel update timer.
       */
      this.clearBuffers = function () {
        self.currentOperationStates = {};
        self.savedOperationStates = [];
        self.addedOperationStatesQ = [];
        self.sentOperationStates = [];
        oldOperationStates = {};
        $timeout.cancel(timeoutPromise);
      }

      this.getSavedStates = function (nid, sid, gid) {
        self.status.updating = true;
        ExperDAPI.getStatesFromSid(nid, sid, gid).success(function (data) {
          self.savedOperationStates = data.states;
          if (self.savedOperationStates.length > 0) {
            self.status.last_received_sid = parseInt(self.savedOperationStates[0].sid, 10);
          }
          updateCurrentOperationStates();
          self.savedOperationStates = [];
          self.status.updating = false;
          self.status.error = false;
          self.status.readonly = data.readonly || false;
          if (self.config.autoUpdate) {
            $timeout.cancel(timeoutPromise);
            timeoutPromise = $timeout(function () {
              self.syncStatesWithDB(nid, self.status.last_received_sid + 1, gid);
            }, self.config.pollingInterval);
          }
        }).error(function(data){
          alert("Error synchronizing workflow data. Changes will not be saved or synchronized. Click Reload button to try again.");
          self.status.updating = false;
          self.status.error = true;
        });
      };

      this.saveStates = function (nid, sid, gid) {
        self.status.updating = true;
        self.status.saving = true;
        var data = {};
//        for (var uuid in self.addedOperationStates) {
//          data.push(self.addedOperationStates[uuid]);
//        }
        data.states = angular.copy(self.addedOperationStatesQ);
        self.sentOperationStates = angular.copy(self.addedOperationStatesQ);
//        for (var uuid in self.sentOperationStates) {
//          self.sentOperationStates[uuid].databaseAction = 'update';
//        }
//        for (var i=0; i<self.sentOperationStates.length; i++) {
//          self.sentOperationStates[i].databaseAction = 'update';
//        }
        self.addedOperationStatesQ = [];
        updateCurrentOperationStates();
        ExperDAPI.addOperationState(nid, data, sid, gid).success(function (data) {
          self.savedOperationStates = data.states;
          if (self.savedOperationStates.length > 0) {
            self.status.last_received_sid = parseInt(self.savedOperationStates[0].sid, 10);
            self.status.last_saved_sid = self.status.last_received_sid;
          }
          self.sentOperationStates = []; // send states should be returned in response
          updateCurrentOperationStates();
          self.savedOperationStates = [];
          self.status.updating = false;
          self.status.saving = false;
          self.status.error = false;
          self.status.readonly = data.readonly || false;
          if (self.config.autoUpdate) {
            $timeout.cancel(timeoutPromise);
            timeoutPromise = $timeout(function () {
              self.syncStatesWithDB(nid, self.status.last_received_sid + 1, gid);
            }, self.config.pollingInterval);
          }
        }).error(function(data){
          alert("Error synchronizing workflow data. Changes will not be saved or synchronized. Click Reload button to try again.");
          self.status.updating = false;
          self.status.error = true;
        });
      };

      /**
       * @ngdoc function
       * @name experdServices.ExperDOperationService#syncStatesWithDB
       * @function
       * @methodOf experdServices.ExperDOperationService
       * @param {int} nid Node id of ExperD node
       * @param {int} sid Get states from sid
       * @param {int} gid Group id of current user
       * @description
       * Synchronize the workflow with the database.
       * 
       */
      this.syncStatesWithDB = function (nid, sid, gid) {
        //if (jQuery.isEmptyObject(self.addedOperationStates)) {
        if (self.addedOperationStatesQ.length === 0) {
          self.getSavedStates(nid, sid, gid);
        } else {
          self.saveStates(nid, sid, gid);
        }
      };

      $window.onbeforeunload = function (event) {
        if (self.addedOperationStatesQ.length > 0) {
          // Try to save unsaved states before unload
          return "WARNING: You have unsaved changes. Stay on this page to save changes to server.";
        }
      };


      /**
       * @ngdoc function
       * @name experdServices.ExperDOperationService#getSnapshots
       * @function
       * @methodOf experdServices.ExperDOperationService
       * @param {int} nid Node id of ExperD node
       * @description
       * Retrieve snapshots for ExperD with id nid.
       * 
       */
      this.getSnapshots = function (nid) {
        ExperDAPI.getSnapshots(nid).success(function (data) {
          self.snapshots = data;
        });
      };

      /**
       * @ngdoc function
       * @name experdServices.ExperDOperationService#saveSnapshot
       * @function
       * @methodOf experdServices.ExperDOperationService
       * @param {string} name Snapshot name
       * @description
       * Save snapshot for current ExperD with name.
       * 
       */
      this.saveSnapshot = function (name) {
        var label = name || "no-name";
        var data = [label];
        ExperDAPI.saveSnapshot(self.xpnid, data).success(function (data) {
          self.getSnapshots(self.xpnid); // update snapshots list
          console.log("Snapshot saved as id: " + data[0]);
        });
      };

      /**
       * @ngdoc function
       * @name experdServices.ExperDOperationService#loadSnapshot
       * @function
       * @methodOf experdServices.ExperDOperationService
       * @param {int} snap_id Snapshot id
       * @param {boolean} autosave Save current schema as snapshot for backup
       * @description
       * Retrieve snapshot for current ExperD with id 'snap_id' and save 
       * as current schema.
       */
      this.loadSnapshot = function (snap_id, autosave) {
        self.clearBuffers();
        getSnapshot(self.xpnid, snap_id, autosave);
      };

      function getSnapshot(nid, snap_id, autosave, gid) {
        self.status.updating = true;
        ExperDAPI.restoreSnapshot(nid, snap_id, autosave, gid).success(function (data) {
          self.savedOperationStates = data.states;
          if (self.savedOperationStates.length > 0) {
            self.status.last_received_sid = parseInt(self.savedOperationStates[0].sid, 10);
          }
          updateCurrentOperationStates();
          self.savedOperationStates = [];
          self.status.updating = false;
          self.status.readonly = data.readonly || false;
          self.getSnapshots(self.xpnid); // update snapshots list

          if (self.config.autoUpdate) {
            $timeout.cancel(timeoutPromise);
            timeoutPromise = $timeout(function () {
              self.syncStatesWithDB(nid, self.status.last_received_sid + 1, gid);
            }, self.config.pollingInterval);
          }
        });
      }

    }
  ]);
})();