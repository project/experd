/**
 * @file Wrapper directive for krytWorkflow jQuery - plugin wrapper for jsPlumb
 */
/**
 * @ngdoc overview
 * @name experdWorkflow
 * @description
 * #ExperD Workflow module
 * The experdWorkflow module implements directives for displaying and editing an
 * ExperD workflow.  
 *   
 * A workflow describes an experiment.
 */

(function () {
  "use strict"; //ECMA5 strict modus
  var experdWorkflow = angular.module('experdWorkflow', []);


  /**
   * @ngdoc directive
   * @name experdWorkflow.directive:krytWorkflow
   * @restrict E
   * @requires $filter
   * @param {string} class classname for element
   * @param {string} id element id
   * @param {expression} figure-definitions expression returning the operation definitions
   * @description 
   * This is a wrapper directive for the krytWorkflow jQuery plugin for jsPlumb.  
   * Changes in the workflow by the user (e.g. adding, deleting or moving a 
   * figure or connecting or disconnecting two figures)  are emitted as **events**
   * on the scope:  
   * - {@link experdWorkflow.directive:krytWorkflow#events_krytWorkflowaddFigure krytWorkflow.addFigure}
   * - {@link experdWorkflow.directive:krytWorkflow#events_krytWorkflowremoveFigure krytWorkflow.removeFigure}
   * - {@link experdWorkflow.directive:krytWorkflow#events_krytWorkflowconnect krytWorkflow.connect}
   * - {@link experdWorkflow.directive:krytWorkflow#events_krytWorkflowdisconnect krytWorkflow.disconnect}
   * - {@link experdWorkflow.directive:krytWorkflow#events_krytWorkflowfigureClick krytWorkflow.figureClick}
   * - {@link experdWorkflow.directive:krytWorkflow#events_krytWorkflowfigureMouseEnter krytWorkflow.figureMouseEnter}
   * - {@link experdWorkflow.directive:krytWorkflow#events_krytWorkflowfigureMouseLeave krytWorkflow.figureMouseLeave}
   * - {@link experdWorkflow.directive:krytWorkflow#events_krytWorkflowmoveFigure krytWorkflow.moveFigure}  
   *   
   * The workflow can also be changed by another client, working on the same workflow.
   * The workflow is notified by a **'displaylist.updated'** event that changes to
   * the workflow are available.
   * The directive listens for this event and updates the workflow accordingly.
   */

  experdWorkflow.directive('krytWorkflow', ['$filter',
    function ($filter) {

      return {
        restrict: 'E',
        controller: function($scope) {
          $scope.zoomValue = 1;
        },
        link: function (scope, element, attrs) {
          var workflow = element.krytWorkflow();
          var krytWorkflow = workflow.data('krytWorkflow');
          var updatingFromGUI = false; // flag to notify $watch function 

          function readonlyMode() {
            return scope.$eval(attrs.isLocked);
          }

          // element.on('mousewheel DOMMouseScroll', _onWheel);

          function _onWheel(event) {
            if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
              scope.zoomValue += 0.05
              // zoom in
            }
            else {
              scope.zoomValue -= 0.05
              // out
            }
            scope.setZoom(scope.zoomValue);
            event.preventDefault();
            scope.$apply();
          }

          /**
           * @ngdoc event
           * @name experdWorkflow.directive:krytWorkflow#krytWorkflow(dot)moveFigure
           * @eventOf experdWorkflow.directive:krytWorkflow
           * @eventType emit on Current krytWorkflow scope
           * @param {string} figureUuid Unique id of moved element
           * @param {int} x Moved to x position
           * @param {int} y Moved to y position
           * @param {boolean} byGUI TRUE if moved by user
           * @description
           * Event name: **'krytWorkflow.moveFigure'**
           * *Names with '.' dont work properly in ngdocs*  
           * Handle moveFigure event on workflow figure.
           */
          workflow.on('krytWorkflow.moveFigure', function (e, figureUuid, x, y, byGUI) {
            //console.log('moveFigure', figureUuid, x, y, byGUI);
            if (byGUI) {
              updatingFromGUI = true;
              // update model
              //scope.$apply(function () {
              scope.$emit('krytWorkflow.moveFigure', figureUuid, x, y, byGUI);
              //});
              updatingFromGUI = false;
            } else {
              scope.$emit('krytWorkflow.moveFigure', figureUuid, x, y, byGUI);
            }
          });

          /**
           * @ngdoc event
           * @name experdWorkflow.directive:krytWorkflow#krytWorkflow(dot)connect
           * @eventOf experdWorkflow.directive:krytWorkflow
           * @eventType emit on Current krytWorkflow scope
           * @param {string} outFigureUuid Unique id of outgoing element
           * @param {int} outPortId Id of output port
           * @param {string} inFigureUuid Unique id of incoming element
           * @param {int} inPortId Id of input port
           * @param {boolean} byGUI TRUE if moved by user
           * @description
           * Event name: **'krytWorkflow.connect'**
           * *Names with '.' dont work properly in ngdocs*  
           * Handle connect event on workflow figure.
           */
          workflow.on('krytWorkflow.connect', function (e, outFigureUuid, outPortId, inFigureUuid, inPortId, byGUI, connection) {
            //console.log('connect', outFigureUuid, outPortId, inFigureUuid, inPortId, byGUI, connection);
            if (byGUI) {
              updatingFromGUI = true;
              //scope.$apply(function () {
              scope.$emit('krytWorkflow.connect', outFigureUuid, outPortId, inFigureUuid, inPortId, byGUI);
              //});
              updatingFromGUI = false;
            } else {
              scope.$emit('krytWorkflow.connect', outFigureUuid, outPortId, inFigureUuid, inPortId, byGUI);
            }
          });

          /**
           * @ngdoc event
           * @name experdWorkflow.directive:krytWorkflow#krytWorkflow(dot)disconnect
           * @eventOf experdWorkflow.directive:krytWorkflow
           * @eventType emit on Current krytWorkflow scope
           * @param {string} outFigureUuid Unique id of outgoing element
           * @param {int} outPortId Id of output port
           * @param {string} inFigureUuid Unique id of incoming element
           * @param {int} inPortId Id of input port
           * @param {boolean} byGUI TRUE if moved by user
           * @description
           * Event name: **'krytWorkflow.disconnect'**
           * *Names with '.' dont work properly in ngdocs*  
           * Handle disconnect event on workflow figure.
           */
          workflow.on('krytWorkflow.disconnect', function (e, outFigureUuid, outPortId, inFigureUuid, inPortId, byGUI, connection) {
            //console.log('disconnect', outFigureUuid, outPortId, inFigureUuid, inPortId, byGUI, connection);
            if (byGUI) {
              updatingFromGUI = true;
              //scope.$apply(function () {
              scope.$emit('krytWorkflow.disconnect', outFigureUuid, outPortId, inFigureUuid, inPortId, byGUI);
              //});
              updatingFromGUI = false;
            } else {
              scope.$emit('krytWorkflow.disconnect', outFigureUuid, outPortId, inFigureUuid, inPortId, byGUI);
            }
          });

          /**
           * @ngdoc event
           * @name experdWorkflow.directive:krytWorkflow#krytWorkflow(dot)removeFigure
           * @eventOf experdWorkflow.directive:krytWorkflow
           * @eventType emit on Current krytWorkflow scope
           * @param {string} figureUuid Unique id of removed element
           * @param {boolean} byGUI TRUE if moved by user
           * @description
           * Event name: **'krytWorkflow.removeFigure'**
           * *Names with '.' dont work properly in ngdocs*  
           * Handle removeFigure event on workflow figure.
           */
          workflow.on('krytWorkflow.removeFigure', function (e, $figure, byGUI) {
            //console.log('removeFigure', $figure);
            if (byGUI) {
              updatingFromGUI = true;
              // update model
              var figureUuid = $figure.attr('id');
              //scope.$apply(function () {
              scope.$emit('krytWorkflow.removeFigure', figureUuid, byGUI);
              //});
              updatingFromGUI = false;
            } else {
              scope.$emit('krytWorkflow.removeFigure', figureUuid, byGUI);
            }
          });

          workflow.on('krytWorkflow.figureDblClick', function (e, $figure, operationNid, uuid) {
            //console.log('figureDblClick', $figure, figureDefinition, uuid);
            //krytWorkflow.removeFigure($figure, true);
            //return true; //return false to prevent figure
          });

          /**
           * @ngdoc event
           * @name experdWorkflow.directive:krytWorkflow#krytWorkflow(dot)figureClick
           * @eventOf experdWorkflow.directive:krytWorkflow
           * @eventType emit on Current krytWorkflow scope
           * @param {string} uuid Unique id of clicked element
           * @description
           * Event name: **'krytWorkflow.figureClick'**
           * *Names with '.' dont work properly in ngdocs*  
           * Handle click event on workflow figure.
           */
          workflow.on('krytWorkflow.figureClick', function (e, $figure, operationNid, uuid) {
//            console.log('figureClick', $figure, figureDefinition, uuid);
            angular.element('.kw_active').removeClass('kw_active');
            $figure.addClass('kw_active');
            scope.$emit('krytWorkflow.figureClick', uuid);
          });

          /**
           * @ngdoc event
           * @name experdWorkflow.directive:krytWorkflow#krytWorkflow(dot)figureMouseEnter
           * @eventOf experdWorkflow.directive:krytWorkflow
           * @eventType emit on Current krytWorkflow scope
           * @param {object} $figure The target figure as jQuery object
           * @param {int} operationNid Node id of operation
           * @param {string} uuid Unique id of clicked element
           * @description
           * Event name: **'krytWorkflow.figureMouseEnter'**
           * *Names with '.' dont work properly in ngdocs*  
           * Handle mouseEnter event on workflow figure.
           */
          workflow.on('krytWorkflow.figureMouseEnter', function (e, $figure, operationNid, uuid) {
            $figure.attr('title', uuid);
            scope.$emit('krytWorkflow.figureMouseEnter', $figure, operationNid, uuid);
            //console.log('figureMouseEnter', $figure, figureDefinition, uuid);
            return true; //return false to prevent figure
          });

          /**
           * @ngdoc event
           * @name experdWorkflow.directive:krytWorkflow#krytWorkflow(dot)figureMouseLeave
           * @eventOf experdWorkflow.directive:krytWorkflow
           * @eventType emit on Current krytWorkflow scope
           * @param {object} $figure The target figure as jQuery object
           * @param {int} operationNid Node id of operation
           * @param {string} uuid Unique id of clicked element
           * @description
           * Event name: **'krytWorkflow.figureMouseLeave'**
           * *Names with '.' dont work properly in ngdocs*  
           * Handle mouseLeave event on workflow figure.
           */
          workflow.on('krytWorkflow.figureMouseLeave', function (e, $figure, operationNid, uuid) {
            scope.$emit('krytWorkflow.figureMouseLeave', $figure, operationNid, uuid);
            //console.log('figureMouseEnter', $figure, figureDefinition, uuid);
            return true; //return false to prevent figure
          });

          /**
           * @ngdoc event
           * @name experdWorkflow.directive:krytWorkflow#krytWorkflow(dot)addFigure
           * @eventOf experdWorkflow.directive:krytWorkflow
           * @eventType emit on Current krytWorkflow scope
           * @param {int} nid Node id of operation
           * @param {string} figureUuid Unique id of removed element
           * @param {boolean} byGUI TRUE if moved by user
           * @description
           * Event name: **'krytWorkflow.addFigure'**
           * *Names with '.' dont work properly in ngdocs*  
           * Handle addFigure event on workflow figure.
           */
          workflow.on('krytWorkflow.addFigure', function (e, nid, uuid, byGUI) {
            //console.log('krytWorkflow.addFigure', nid, uuid, byGUI);
            if (byGUI) {
              updatingFromGUI = true;
              //scope.$apply(function () {
              scope.$emit('krytWorkflow.addFigure', nid, uuid, byGUI);
              //});
              updatingFromGUI = false;
            } else {
              scope.$emit('krytWorkflow.addFigure', nid, uuid, byGUI);
            }
          });

          /**
           * @ngdoc function
           * @name experdWorkflow.directive:krytWorkflow#addFigure
           * @methodOf experdWorkflow.directive:krytWorkflow
           * @function 
           * @param {int} nid Node id of operation to add
           * @param {object} location Position in workflow where operation is added
           * @returns {string} uuid of added operation
           * @description 
           * Add figure to krytWorkflow.  
           * Function is added to the scope, so it can be called from a view
           */
          scope.addFigure = function (nid, location) {
            if (readonlyMode())
              return;
            var uuid = krytWorkflow.addFigure(parseInt(nid, 10), undefined, true); // byGUI = true
            if (angular.isObject(location)) {
//              newOperation.plugins.experd.location = location;
              // moveFigure is called 2 times because in this case the move
              // is both byGUI and not byGUI
              // move the figure in the workflow 
              // byGUI=false => figure is moved, no event emitted to controller
              krytWorkflow.moveFigure(uuid, location.x, location.y);  // byGUI = false
              // pretend that the figure was dragged by the user
              // byGUI=true => figure not moved, event emitted to controller
              krytWorkflow.moveFigure(uuid, location.x, location.y, true);  // byGUI = true
            }
            return uuid;
          };

          /**
           * @ngdoc function
           * @name experdWorkflow.directive:krytWorkflow#deleteFigure
           * @methodOf experdWorkflow.directive:krytWorkflow
           * @function 
           * @param {string} uuid Unique id of figure to remove from workflow
           * @param {object} location Position in workflow where operation is added
           * @description 
           * Remove figure from krytWorkflow  
           * Function is added to the scope, so it can be called from a view
           */
          scope.deleteFigure = function (uuid) {
            krytWorkflow.removeFigure(uuid, true);
          };

          /**
           * @ngdoc function
           * @name experdWorkflow.directive:krytWorkflow#repaint
           * @methodOf experdWorkflow.directive:krytWorkflow
           * @function 
           * @description 
           * Repaint krytWorkflow. Redraws the connections and endpoints in the 
           * workflow.  
           * Function is added to the scope, so it can be called from a view.
           */
          scope.repaint = function () {
            krytWorkflow.repaint();
          };

          /**
           * @ngdoc function
           * @name experdWorkflow.directive:krytWorkflow#clearWorkflow
           * @methodOf experdWorkflow.directive:krytWorkflow
           * @function 
           * @description 
           * Clear content of the worflow.  
           * Function is added to the scope, so it can be called from a view.
           */
          scope.clearWorkflow = function () {
            //workflow.find(".kw_figure").remove();
            workflow.find(".kw_figure").each(function () {
              var uuid = this.id;
              krytWorkflow.removeFigure(uuid);
            });
            krytWorkflow.clear();
          };

          scope.setZoom = function (zoom) {
            krytWorkflow.setZoom(zoom);
          };

          scope.$watch(attrs.figureDefinitions, function (value, oldValue) {
            krytWorkflow.updateFigureDefinitions(value);
          });

          //scope.$watch(attrs.displayList, function (value, oldValue) {
          //scope.$watchCollection(attrs.displayList, function (value, oldValue) {

          //$on displaylist.update event send from ExperDOperationService
          /**
           * @ngdoc function
           * @name experdWorkflow.directive:krytWorkflow#updateDisplaylistHandler
           * @function updateDisplaylistHandler
           * @methodOf experdWorkflow.directive:krytWorkflow
           * @param {Object} event The original event
           * @param {Object} value List of all operations in workflow
           * @param {Object} oldValue Previous list of all operations in workflow
           * @param {Array} updatedUuids Array of all updated uuid's in value object
           * @param {Array} deletedUuids Array of uuid's of all deleted operations
           * @description
           * Handler for 'displaylist.updated' event.
           */
          function updateDisplaylistHandler(event, value, oldValue, updatedUuids, deletedUuids) {
            if (updatingFromGUI || (updatedUuids.length === 0 && deletedUuids.length === 0)) {
              // no need to update (already done by GUI)
              return;
            }
            krytWorkflow.setSuspendDrawing(true);

            // add new states
            //for (var uuid in value) {
            for (var i = 0; i < updatedUuids.length; i++) {
              var uuid = updatedUuids[i];
              var state = value[uuid];
              if (state.databaseAction !== 'delete' && state.plugins.experd.action !== 'delete' && workflow.find("#" + uuid).length === 0) {
                var nid = parseInt(state.operationDbNid, 10); // get operation type nid
                krytWorkflow.addFigure(nid, uuid);
              }
            }

            //remove deleted states
            for (var i = 0; i < deletedUuids.length; i++) {
              var uuid = deletedUuids[i];
              if (workflow.find("#" + uuid).length > 0) {
                krytWorkflow.removeFigure(uuid);
              }
            }

            //remove states from workflow that have no state in the displaylist
//            workflow.find(".kw_figure").each(function () {
//              var uuid = this.id;
//              var state = value[uuid];
//              if (!state || state.databaseAction === 'delete' || state.plugins.experd.action === 'delete') {
//                krytWorkflow.removeFigure(uuid);
//              }
//            });

            // update changed states
            // need to add and delete states first, because we can not make 
            // connections to states that are not yet created
//            for (var uuid in value) {
            for (var i = 0; i < updatedUuids.length; i++) {
              var uuid = updatedUuids[i];
              var state = value[uuid];
              var oldState = oldValue[uuid];
              if (!angular.equals(state, oldState)) { // state has changed
                angular.forEach(state.plugins, function (pluginData, pluginName) {
                  var oldPluginData = oldState && oldState.plugins ? oldState.plugins[pluginName] : {};
                  if (!angular.equals(pluginData, oldPluginData)) { // only changed plugindata
                    scope.$emit("plugin.update." + pluginName, uuid, pluginName, pluginData, oldPluginData);
                  }
                  if (pluginName === "experd") { // handle experd data
                    var oldPos = workflow.find("#" + uuid).position();
                    if (oldPos) {
                      var xpos = parseInt(pluginData.location.x, 10);
                      var ypos = parseInt(pluginData.location.y, 10);
                      if (xpos !== oldPos.left || ypos !== oldPos.top) {
                        krytWorkflow.moveFigure(uuid, xpos, ypos);
                      }
                      //var oldState = $filter('filter')(oldValue, {operationJsNid: uuid})[0];

                      var connectionList = krytWorkflow.getConnectionsFor(uuid);
                      if (!angular.equals(pluginData.connectionsOut, connectionList)) {
                        //remove old connections
                        for (var j = 0; j < connectionList.length; j++) {
                          var connection = connectionList[j];
                          if ($filter('filter')(pluginData.connectionsOut,
                                  {
                                    sourcePortId: connection.sourcePortId,
                                    targetOperationJsNid: connection.targetOperationJsNid,
                                    targetPortId: connection.targetPortId
                                  }).length === 0) {
                            krytWorkflow.disconnect(uuid, connection.sourcePortId, connection.targetOperationJsNid, connection.targetPortId);
                          }
                        }
                        //add new connections
                        for (var j = 0; j < pluginData.connectionsOut.length; j++) {
                          var connection = pluginData.connectionsOut[j];
                          if (workflow.find("#" + connection.targetOperationJsNid).length !== 0) { // target exists
                            krytWorkflow.connect(uuid, connection.sourcePortId, connection.targetOperationJsNid, connection.targetPortId);
                          }
                        }
                      }
                    }
                  }
                });
              }
            }

            krytWorkflow.setSuspendDrawing(false);
          }
          ;
          /**
           * @ngdoc event
           * @name experdWorkflow.directive:krytWorkflow#plugin(dot)update(dot)pluginname
           * @eventOf experdWorkflow.directive:krytWorkflow
           * @eventType emit on Current krytWorkflow scope
           * uuid, pluginName, pluginData, oldPluginData
           * @param {string} uuid Unique id of removed element
           * @param {string} pluginName Name of plugin
           * @param {object} pluginData Updated plugindata
           * @param {object} oldPluginData Previous plugindata
           * @description
           * Event name: **'plugin.update.{_pluginname_}'**
           * *Names with '.' dont work properly in ngdocs*  
           * Emitted by krytWorkflow if new data for a plugin is avalaible.
           */

          /**
           * Listen for 'displaylist.updated' event.
           */
          scope.$on('displaylist.updated', updateDisplaylistHandler);
//          }, true);

        }
      };
    }]);

  /*
   * Directives for adding figures with drag and drop support using jQueryUI wrappers
   */

  /**
   * @ngdoc directive
   * @name experdWorkflow.directive:kwDraggable
   * @restrict A
   * @description
   * Wrapper directive for jQueryUI draggable<br>
   * Add this attribute to the experdOperation elements in the workflow toolbar
   * to enable drag and drop support.
   */
  experdWorkflow.directive('kwDraggable', function () {
    return {
      // A = attribute, E = Element, C = Class and M = HTML Comment
      restrict: 'A',
      link: function (scope, element, attrs) {
        element.draggable({
          revert: false,
          helper: 'clone',
          appendTo: 'body',
          start: function (event, ui) {
          },
          stop: function (event, ui) {
          }
        });
      }
    };
  });

  /**
   * @ngdoc directive
   * @name experdWorkflow.directive:kwDroppable
   * @restrict A
   * @description
   * Wrapper directive for jQueryUI droppable.<br>
   * When an .experdOperation element is dropped it is added to the workflow
   * and moved to the drop location.<br>
   * Add this attribute to the kryt-workflow element to enable drag and drop support.
   * 
   */
  experdWorkflow.directive('kwDroppable', function () {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        element.droppable({
          accept: ".experdOperation",
          drop: function (event, ui) {
            //console.log('dropped', event, ui, ui.draggable.attr('id'));
            var nid = ui.draggable.attr('id');
            var offset = angular.element(this).offset();
            var zoom = scope.zoomValue || 1;
            var location = {x: (ui.offset.left - offset.left) / zoom, y: (ui.offset.top - offset.top) / zoom};
            var uuid = scope.addFigure(nid, location);
          }
        });
      }
    };
  });
})();
