1. Figure definitions
------------------------

[
  {
    "<drupalNid>": {
      "name": "<name>",
      "description": "<description>",
      "resourceUrls": [
        "<url>"
      ],
      "category": "<categoryId>",
      "formTemplate": "<uri> OR <HTML snipplet>",
      "outputPorts": [
        {
          "name": "<name>",
          "position": "n|w|e|s"
        }
      ],
      "inputPorts": [
        {
          "name": "<name>"
        }
      ],
      "imageUrl": "SimulationOfTheMashingProcess.png"
    }
  }
]

2. Figure configurations
------------------------