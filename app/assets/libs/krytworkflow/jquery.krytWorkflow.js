/**
 * krytWorkflow
 * based on boilerplate version 1.3
 * @param {object} $ A jQuery object
 **/
(function ($) {
  "use strict"; //ECMA5 strict modus

  $.krytWorkflow = function (element, settings) {

    /* define vars
     */

    /* this object will be exposed to other objects */
    var publicObj = this;

    //the version number of the plugin
    publicObj.version = '1.0';

    /* this object holds functions used by the plugin boilerplate */
    var _helper = {
      /**
       * Call hooks, additinal parameters will be passed on to registered plugins
       * @param {string} name
       */
      "doHook": function (name) {
        var i;
        var returnValue = true;
        var pluginFunctionArgs = [];

        /* remove first two arguments */
        for (i = 1; i < arguments.length; i++) {
          pluginFunctionArgs.push(arguments[i]);
        }

        /* call plugin functions */
        if (_globals.plugins !== undefined) {
          /* call plugins */
          $.each(_globals.plugins, function (_, extPlugin) {
            if (extPlugin.__hooks !== undefined && extPlugin.__hooks[name] !== undefined) {
              returnValue = (returnValue === false || extPlugin.__hooks[name].apply(publicObj, pluginFunctionArgs) === false) ? false : true;
            }
          });
        }

        /* trigger event on main element */

        returnValue = (returnValue === false || _globals.$element.triggerHandler('krytWorkflow.' + name, pluginFunctionArgs) === false) ? false : true;

        return returnValue;
      },
      /**
       * Initializes the plugin
       */
      "doInit": function () {
        _helper.doHook('beforeInit', publicObj, element, settings);
        publicObj.init();
        _helper.doHook('init', publicObj);
      },
      /**
       * Loads an external script
       * @param {string} libName
       * @param {string} errorMessage
       */
      "loadScript": function (libName, errorMessage) {
        /* remember libname */
        _cdnFilesToBeLoaded.push(libName);

        /* load script */
        $.ajax({
          "type": "GET",
          "url": _globals.dependencies[libName].cdnUrl,
          "success": function () {
            /* forget libname */
            _cdnFilesToBeLoaded.splice(_cdnFilesToBeLoaded.indexOf(libName), 1); //remove element from _cdnFilesToBeLoaded array

            /* call init function when all scripts are loaded */
            if (_cdnFilesToBeLoaded.length === 0) {
              _helper.doInit();
            }
          },
          "fail": function () {
            console.error(errorMessage);
          },
          "dataType": "script",
          "cache": "cache"
        });
      },
      /**
       * Registers a plugin
       * @param {string} name Name of plugin, must be unique
       * @param {object} object An object {("functions": {},) (, "hooks: {})}
       */
      "registerPlugin": function (name, object) {
        var plugin;
        var hooks;

        /* reorder plugin */
        hooks = $.extend(true, {}, object.hooks);
        plugin = object.functions !== undefined ? object.functions : {};
        plugin.__hooks = hooks;

        /* add plugin */
        _globals.plugins[name] = plugin;
      },
      /**
       * Calls a plugin function, all additional arguments will be passed on
       * @param {string} krytWorkflow
       * @param {string} pluginFunctionName
       */
      "callPluginFunction": function (krytWorkflow, pluginFunctionName) {
        var i;

        /* remove first two arguments */
        var pluginFunctionArgs = [];
        for (i = 2; i < arguments.length; i++) {
          pluginFunctionArgs.push(arguments[i]);
        }

        /* call function */
        _globals.plugins[krytWorkflow][pluginFunctionName].apply(null, pluginFunctionArgs);
      },
      /**
       * Checks dependencies based on the _globals.dependencies object
       * @returns {boolean}
       */
      "checkDependencies": function () {
        var dependenciesPresent = true;
        for (var libName in _globals.dependencies) {
          var errorMessage = 'jquery.krytWorkflow: Library ' + libName + ' not found! This may give unexpected results or errors.';
          var doesExist = $.isFunction(_globals.dependencies[libName]) ? _globals.dependencies[libName] : _globals.dependencies[libName].doesExist;
          if (doesExist.call() === false) {
            if ($.isFunction(_globals.dependencies[libName]) === false && _globals.dependencies[libName].cdnUrl !== undefined) {
              /* cdn url provided: Load script from external source */
              _helper.loadScript(libName, errorMessage);
            } else {
              console.error(errorMessage);
              dependenciesPresent = false;
            }
          }
        }
        return dependenciesPresent;
      }
    };
    /* keeps track of external libs loaded via their CDN */
    var _cdnFilesToBeLoaded = [];

    /* this object holds all global variables */
    var _globals = {};

    //holds the jsPlumb object
    _globals.jsPlumb = null;

    //holds the jsPlumb settings
    _globals.jsPlumbSettings = {
      "endPoints": {
        "target": {
          "endpoint": "Rectangle",
          "paintStyle": {
            "width": 40,
            "height": 15
          },
          "isSource": false,
          "isTarget": true,
          "connectorClass": "kw_connection",
          "cssClass": "kw_port kw_port_in",
          "maxConnections": -1
        },
        "source": {
          "endpoint": "Rectangle",
          "paintStyle": {
            "width": 40,
            "height": 15
          },
          "isSource": "true",
          "isTarget": false,
          "connectorClass": "kw_connection",
          "cssClass": "kw_port kw_port_out",
          "maxConnections": -1
        }
      }
    };

    //the figure definitions
    _globals.figureDefinitions = null;

    //this object holds references to all endpoints, @see function registerEndpoint
    _globals.endPoints = {};

    //this object holds references to all connections, @see function registerConnection
    _globals.connections = {};

    /* handle settings */
    var defaultSettings = {
      "figureDefinitions": {},
      "showConnectionLabels": true,
      "idPrefixes": {
        "figure": ""
//        ,
//        "connection": "kw_connection_",
//        "connectionLabel": "kw_connection_label_",
//        "connector": "kw_connector_",
//        "connectorLabel": "kw_connector_label_"
      }
    };

    _globals.settings = {};

    if ($.isPlainObject(settings) === true) {
      _globals.settings = $.extend(true, {}, defaultSettings, settings);
    } else {
      _globals.settings = defaultSettings;
    }

    /* this object contains a number of functions to test for dependencies,
     * doesExist function should return TRUE if the library/browser/etc is present
     */
    _globals.dependencies = {
      /* check for jQuery 1.6+ to be present */
      "jquery1.6+": {
        "doesExist": function () {
          var jqv, jqv_main, jqv_sub;
          if (window.jQuery) {
            jqv = jQuery().jquery.split('.');
            jqv_main = parseInt(jqv[0], 10);
            jqv_sub = parseInt(jqv[1], 10);
            if (jqv_main > 1 || (jqv_main === 1 && jqv_sub >= 6)) {
              return true;
            }
          }
          return false;
        },
        "cdnUrl": "http://code.jquery.com/jquery-git1.js"
      },
      "jsPlumb.1.7": {
        "doesExist": function () {
          if (typeof jsPlumb !== undefined) {
            return true;
          } else {
            return false;
          }
        }
      }
    };
    _helper.checkDependencies();

    //this object holds all plugins
    _globals.plugins = {};



    /* register DOM elements
     * jQuerified elements start with $
     */
    _globals.$element = $(element);

    /**
     * Init function
     **/
    publicObj.init = function () {
      _globals.jsPlumb = jsPlumb.getInstance();

      /* add event listeners */
      addJSPlumbEventListeners();

      /* set the container */
      _globals.jsPlumb.setContainer(element);
      if (settings && settings.figureDefinitions) {
        setFigureDefinitions(settings.figureDefinitions);
      }

    };

    /**
     * Sets whether drawing/refreshing of canvas should be suspended (for performance reasons)
     * @param {type} flag
     */
    publicObj.setSuspendDrawing = function (flag) {
      _globals.jsPlumb.setSuspendDrawing(flag);
    };

    /**
     * Adds an figure to the workflow
     * @param {mixed} figReference Either {integer} figures nid, as defined in the figure definitions
     *                           or {object} an figure configuration.
     * @param {string} uuid (optional) The uuid of the figure
     * @return string The uuid of the figure.
     */
    publicObj.addFigure = function (figReference, uuid, byGUI) {
      var byGUI = byGUI === undefined ? false : byGUI;
      if (_globals.isDragging && !byGUI)
        return; //dont update while dragging connection
      var $newFigure, $widgetsContainer;
      var figDefinition, figImg = '';
      var uuid = uuid === undefined ? _globals.settings.idPrefixes.figure + getUniqueId() : uuid;


      switch (typeof figReference) {
        case "number":
          figReference = figReference.toString();
          figDefinition = $.extend(true, {}, _globals.figureDefinitions[figReference]);
          figDefinition.widgets = figDefinition.widgets === undefined ? [] : figDefinition.widgets;
          if (_helper.doHook('beforeAddFigure', figDefinition, uuid, byGUI) === false) {
            return;
          }

          if (figDefinition !== undefined) {
            /*
             * Create new figure and att it to workflow
             */
            $newFigure = $('<div class="kw_figure" id="' + uuid + '"><div');
            if (figDefinition.category !== undefined) {
              $newFigure.addClass();
            }
            $newFigure.append('<div class="kw_figure_name kw_color' + figDefinition.category + '">' + figDefinition.name + '</div>');

            if (figDefinition.imageUrl) {
              figImg = '<img src="' + figDefinition.imageUrl + '" alt="' + figDefinition.name + '" />';
            }
            $newFigure.append('<div class="kw_figure_image">' + figImg + '</div>');

            /* add widgets */
            $widgetsContainer = $('<div class="kw_figure_widgets"></div>');
            $newFigure.append($widgetsContainer);

            $.each(figDefinition.widgets, function (wi) {
              $widgetsContainer.append(figDefinition.widgets[wi].element);
            });

            _helper.doHook('beforeAppendFigure', figReference, uuid, $newFigure, byGUI);
            _globals.$element.append($newFigure);

            /**
             * Make figure dragable
             */
            var dragStartPos = [];
            _globals.jsPlumb.draggable($newFigure, {
              "containment": "parent",
              "start": function (plumbEvent) {
                dragStartPos = plumbEvent.pos;
              },
              "stop": function (plumbEvent) {
                // this event is also triggered on click, without moving
                // compare start and end positions to detect movement
                if (dragStartPos && (dragStartPos[0] !== plumbEvent.pos[0] || dragStartPos[1] !== plumbEvent.pos[1])) {
                  publicObj.moveFigure(plumbEvent.el.id, plumbEvent.pos[0], plumbEvent.pos[1], true);
                }
                dragStartPos = [];
              }
            });

            /**
             * Add event listeners to figure
             */
            $newFigure.on('click', function () {
              _helper.doHook('figureClick', $(this), figDefinition, uuid);
            });

            $newFigure.on('touchend', function () {
              _helper.doHook('figureClick', $(this), figDefinition, uuid);
            });

            $newFigure.on('dblclick', function () {
              _helper.doHook('figureDblClick', $(this), figDefinition, uuid);
            });

            $newFigure.on('mouseenter', function () {
              _helper.doHook('figureMouseEnter', $(this), figDefinition, uuid);
            });

            $newFigure.on('mouseleave', function () {
              _helper.doHook('figureMouseLeave', $(this), figDefinition, uuid);
            });

            /**
             * Set up endpoints on the divs
             **/
            setPorts($newFigure, 'n', figDefinition.inputPorts);
            setPorts($newFigure, 's', figDefinition.outputPorts);
          }
          break;

        case "object":
          figDefinition = figReference;
          if (_helper.doHook('beforeAddFigure', figDefinition, uuid, byGUI) === false) {
            return;
          }

          break;
      }

      _helper.doHook('addFigure', figReference, uuid, byGUI);
      return uuid;

    };

    /**
     * Removes a figure from the DOM, along with all connections.
     * @param {mixed} ref The figures uuid, the DOM object or the jQuerified DOM object
     */
    publicObj.removeFigure = function (ref, byGUI) {
      byGUI = byGUI === undefined ? false : byGUI;
      if (_globals.isDragging && !byGUI)
        return; //dont update while dragging connection
      var $ref;
      if (typeof ref === 'string') {
        $ref = $('#' + ref);
      } else {
        $ref = ref;
      }
      _helper.doHook('beforeRemoveFigure', ref);

      var outConnections = _globals.jsPlumb.getConnections({"source": ref});
      $.each(outConnections, function (i) {
        var connection = outConnections[i];
        _globals.jsPlumb.detach(outConnections[i], {"fireEvent": false});
        //publicObj.disconnect(outFigureUuid, outPortId, inFigureUuid, inPortId, connection);
      });
      //console.log(_globals.jsPlumb.getConnections({"target": ref}));
      var inConnections = _globals.jsPlumb.getConnections({"target": ref});
      $.each(inConnections, function (i) {
        var connection = inConnections[i];
        _globals.jsPlumb.detach(inConnections[i], {"fireEvent": false});
        //publicObj.disconnect(outFigureUuid, outPortId, inFigureUuid, inPortId, connection);
      });

      _globals.jsPlumb.remove($ref);
      _helper.doHook('removeFigure', $ref, byGUI);
    };

    /**
     * Moves a figure
     * @param {string} figureUuid
     * @param {int} x
     * @param {int} y
     * @param {boolean} byGUI Should be true if figure is moved by user/GUI
     */
    publicObj.moveFigure = function (figureUuid, x, y, byGUI) {
      byGUI = byGUI === undefined ? false : byGUI;
      if (_globals.isDragging && !byGUI)
        return; //dont update while dragging connection
      _helper.doHook('beforeMoveFigure', figureUuid, x, y, byGUI);
      if (byGUI === false) {
        /* move figure to new location */
        //$('#' + figureUuid).css({"top": y, "left": x});
        _globals.$element.find('#' + figureUuid).css({"top": y, "left": x});

        /* repaint its connections */
        _globals.jsPlumb.repaint(figureUuid, {"top": y, "left": x});
      }
      _helper.doHook('moveFigure', figureUuid, x, y, byGUI);
    };


    /**
     * Connects two figures
     * @param {string} outFigureUuid
     * @param {string} outPortId
     * @param {string} inFigureUuid
     * @param {string} inPortId
     * @param {object} connection Optional: de connection object. This is provided when connection is established by the GUI a.k.a. the user.
     */
    publicObj.connect = function (outFigureUuid, outPortId, inFigureUuid, inPortId, connection) {
      var byGUI = arguments.length === 5 ? true : false;
      if (_globals.isDragging && !byGUI)
        return; //dont update while dragging connection
      if (!byGUI && publicObj.hasConnection(outFigureUuid, outPortId, inFigureUuid, inPortId)) {
        //already connected: do nothing;
        return;
      }
      if (_helper.doHook('beforeConnect', outFigureUuid, outPortId, inFigureUuid, inPortId, byGUI, connection) === true) {
        if (byGUI === false) {
          connection = _globals.jsPlumb.connect({
            "source": getEndPoint(outFigureUuid, outPortId),
            "target": getEndPoint(inFigureUuid, inPortId)
          });
        }

//        if (byGUI && publicObj.hasConnection(outFigureUuid, outPortId, inFigureUuid, inPortId) && connection) {
//          // do not allow double connections
//          _globals.jsPlumb.detach(connection);
//          console.log("Double connection deleted");
//        }
//
        if (connection) {
          registerConnection(outFigureUuid, outPortId, inFigureUuid, inPortId, connection);
        }

        _helper.doHook('connect', outFigureUuid, outPortId, inFigureUuid, inPortId, byGUI, connection);
      }

    };


    /**
     * Disonnects two figures
     * @param {string} outFigureUuid
     * @param {string} outPortId
     * @param {string} inFigureUuid
     * @param {string} inPortId
     * @param {object} connection
     */
    publicObj.disconnect = function (outFigureUuid, outPortId, inFigureUuid, inPortId, connection) {
      var byGUI = arguments.length === 5 ? true : false;
      if (_globals.isDragging && !byGUI)
        return; //dont update while dragging connection
      if (!byGUI && !publicObj.hasConnection(outFigureUuid, outPortId, inFigureUuid, inPortId)) {
        //no connection found: do nothing;
        return;
      }
      _helper.doHook('beforeDisconnect', outFigureUuid, outPortId, inFigureUuid, inPortId, byGUI, connection);
      if (byGUI === false) {
        connection = getConnection(outFigureUuid, outPortId, inFigureUuid, inPortId);
        if (connection) {
          _globals.jsPlumb.detach(connection);
        }
      }

      unregisterConnection(outFigureUuid, outPortId, inFigureUuid, inPortId);
      _helper.doHook('disconnect', outFigureUuid, outPortId, inFigureUuid, inPortId, byGUI, connection);
    };

    publicObj.hasConnection = function (outFigureUuid, outPortId, inFigureUuid, inPortId) {
      return (getConnection(outFigureUuid, outPortId, inFigureUuid, inPortId) ? true : false);
    };

    publicObj.getConnectionsFor = getConnectionsFor;

    /**
     * Registers a plugin
     * @param {string} name Name of plugin, must be unique
     * @param {object} object An object {("functions": {},) (, "hooks: {}) (, "targetkrytWorkflows": [])}
     */
    publicObj.registerPlugin = function (name, object) {
      _helper.registerPlugin(name, object);
    };

    /**
     * Sets the figure definitions of the figures which can be added to this workflow
     * @param {object} figureDefinitions {"<drupalNid>": {"name": "string", ...}, ...} See readme.txt
     */
    function setFigureDefinitions(figureDefinitions) {
      _globals.figureDefinitions = figureDefinitions;
    }

    publicObj.updateFigureDefinitions = setFigureDefinitions;

    /**
     * Adds ports to an operation
     * @param {object} $figure The figure jQuery-opbject
     * @param {string} position n|s|e|w
     * @param {array} definitions Port definitions
     */
    function setPorts($figure, position, definitions) {
      var endpointSettings;
      var specificSettings = {
        "overlays": [],
        "connectorOverlays": [["Arrow", {"width": 10, "length": 10, "location": 1, "id": ""}]],
        "connector": ["Flowchart", {"stub": [10, 10], "gap": 10, "cornerRadius": 5, "alwaysRespectStubs": true}],
        "connectorStyle": {
          "lineWidth": 4,
          "strokeStyle": "#444"
        },
        "connectorHoverStyle": {
          "lineWidth": 4,
          strokeStyle: "#ccc"
        }
      };

      /* show connection label? */
      if (_globals.settings.showConnectionLabels === true) {
        specificSettings.connectorOverlays.push(["Label", {"label": '<div class="kw_connection_label"></div>'}]);
      }

      /* add the ports */
      var numberOfDefinitions = definitions.length;
      $.each(definitions, function (i) {
        var definition = definitions[i];
        var relPos = numberOfDefinitions > 1 ? 0.2 + (i / (numberOfDefinitions - 1) * 0.6) : 0.5;

        /* add label */
        if (definition.name) {
          specificSettings.overlays[0] = ["Label", {"label": definition.name, "cssClass": 'kw_port_label kw_port_label_' + position, "id": definition.nid}];
        } else {
          delete specificSettings.overlays[0];
        }


        /* add port */
        switch (position) {
          case 's':
            specificSettings.anchor = [relPos, 1.10, 0, 1];
            endpointSettings = $.extend({}, _globals.jsPlumbSettings.endPoints.source, specificSettings);
            break;

          case 'e':
            specificSettings.anchor = [1, relPos, 1, 0];
            endpointSettings = $.extend({}, _globals.jsPlumbSettings.endPoints.source, specificSettings);
            break;

          case 'n':
            specificSettings.anchor = [relPos, -0.07, 0, -1];
            endpointSettings = $.extend({}, _globals.jsPlumbSettings.endPoints.target, specificSettings);
            break;

          case 'w':
            specificSettings.anchor = [1, relPos, -1, 0];
            endpointSettings = $.extend({}, _globals.jsPlumbSettings.endPoints.source, specificSettings);
            break;
        }

        /* add and register the port, so we can easily retrieve it */
        var e = _globals.jsPlumb.addEndpoint($figure, endpointSettings);
//        e.setLabel({"label": definition.name, "cssClass": 'kw_port_label kw_port_label_' + position});
//        console.log(e.getLabel());
        registerEndPoint($figure.attr('id'), definition.nid, e);
      });
    }

    /**
     * Suspends or unsuspends drawing. Greatly improves performance when adding many figures/connections.
     *  Does a full repaint when flag is set to 'false'
     * @param {boolean} flag
     */
    publicObj.setSuspendDrawing = function (flag) {
      _globals.jsPlumb.setSuspendDrawing(flag, true);
    };

    publicObj.repaint = function () {
      _globals.jsPlumb.repaintEverything();
    };

    publicObj.clear = function () {
      _globals.jsPlumb.deleteEveryEndpoint();
    };

    publicObj.setZoom = function (zoom, instance, transformOrigin, el) {
      transformOrigin = transformOrigin || [0, 0];
      instance = instance || _globals.jsPlumb;
      el = el || instance.getContainer();
      var p = ["webkit", "moz", "ms", "o"],
              s = "scale(" + zoom + ")",
              oString = (transformOrigin[0] * 100) + "% " + (transformOrigin[1] * 100) + "%";

      for (var i = 0; i < p.length; i++) {
        el.style[p[i] + "Transform"] = s;
        el.style[p[i] + "TransformOrigin"] = oString;
      }

      el.style["transform"] = s;
      el.style["transformOrigin"] = oString;

      instance.setZoom(zoom);
    };

    /**
     * Registers an endpoint
     * @param {string} figureUid
     * @param {string} nid
     * @param {object} obj EndPoint object
     */
    function registerEndPoint(figureUid, nid, obj) {
      _globals.endPoints[figureUid + '_' + nid] = obj;
    }

    /**
     * Returns an endpoint
     * @param {string} figureUid
     * @param {string} nid
     */
    function getEndPoint(figureUid, nid) {
      return _globals.endPoints[figureUid + '_' + nid];
    }


    /**
     * Unregisters endpoint
     * @param {string} figureUid
     * @param {string} nid
     */
    function unregisterEndPoint(figureUid, nid) {
      _globals.endPoints[figureUid + '_' + nid] = null;
      delete _globals.endPoints[figureUid + '_' + nid];
    }

    /**
     * Registers a connection
     * @param {string} outFigureUuid
     * @param {string} outPortId
     * @param {string} inFigureUuid
     * @param {string} inPortId
     * @param {object} obj Connection object
     */
    function registerConnection(outFigureUuid, outPortId, inFigureUuid, inPortId, obj) {
      _globals.connections[outFigureUuid + '_' + outPortId + '_' + inFigureUuid + '_' + inPortId] = obj;
      //console.log('* register', _globals.connections);
    }

    /**
     * Returns a connection
     * @param {string} outFigureUuid
     * @param {string} outPortId
     * @param {string} inFigureUuid
     * @param {string} inPortId
     */
    function getConnection(outFigureUuid, outPortId, inFigureUuid, inPortId) {
      var connectionList = _globals.jsPlumb.getConnections({source: outFigureUuid, target: inFigureUuid});
      var result = [];
      for (var i = 0; i < connectionList.length; i++) {
        var foundOutport = getPortIdFromEndPoint(connectionList[i].endpoints[0]);
        var foundInport = getPortIdFromEndPoint(connectionList[i].endpoints[1]);
        if (foundOutport == outPortId && foundInport == inPortId) {
          result.push(connectionList[i]);
        }
      }
      if (result.length > 1) {
        console.log('Double connection detected: ' + outFigureUuid + ' :: ' + outPortId + ' :: ' + inFigureUuid + ' :: ' + inPortId);
      }
      return result[0]; // _globals.connections[outFigureUuid + '_' + outPortId + '_' + inFigureUuid + '_' + inPortId];
    }


    function getConnectionsFor(outFigureUuid) {
      var connectionList = _globals.jsPlumb.getConnections({source: outFigureUuid});
      var result = [];
      for (var i = 0; i < connectionList.length; i++) {
        var outPort = getPortIdFromEndPoint(connectionList[i].endpoints[0]);
        var inPort = getPortIdFromEndPoint(connectionList[i].endpoints[1]);
        var connection = {
          sourcePortId: outPort,
          targetOperationJsNid: connectionList[i].targetId,
          targetPortId: inPort
        }
        result.push(connection);
      }
      return result;
    }

    /**
     * Unregisters a connection
     * @param {string} outFigureUuid
     * @param {string} outPortId
     * @param {string} inFigureUuid
     * @param {string} inPortId
     */
    function unregisterConnection(outFigureUuid, outPortId, inFigureUuid, inPortId) {
      _globals.connections[outFigureUuid + '_' + outPortId + '_' + inFigureUuid + '_' + inPortId] = null;
      delete _globals.connections[outFigureUuid + '_' + outPortId + '_' + inFigureUuid + '_' + inPortId];
      //console.log('* unregister', outFigureUuid + '_' + outPortId + '_' + inFigureUuid + '_' + inPortId, _globals.connections);
    }

    /**
     * Helper function to obtain PortIds from connection
     * @param {object} info A jsplumb info object
     **/
    function getPortIdsFromInfo(info) {
      /* try to get source endpoint label name */
      var sourceEndpoint = info.connection.endpoints[0];
      var targetEndpoint = !info.dropEndpoint ? info.connection.endpoints[1] : info.dropEndpoint;

      return {"inputPortId": getPortIdFromEndPoint(targetEndpoint), "outputPortId": getPortIdFromEndPoint(sourceEndpoint)};
    }

    /**
     * Helper function, gets portId from jsPlumb endpoint
     * @param {object} endpoint A jsPlumb endpoint object
     * @returns string (or: undefined)
     */
    function getPortIdFromEndPoint(endpoint) {
      var i;
      var endpointOverlays = endpoint.getOverlays();
      for (i in endpointOverlays) {
        if (endpointOverlays[i].type === 'Label') {
          return endpointOverlays[i].id;
        }
      }
//      for (i = 0; i < endpointOverlays.length; i++) {
//        if (endpointOverlays[i].type === 'Label') {
//          return endpointOverlays[i].id;
//        }
//      }
    }

    /**
     * Helper function. Adds event listeners to jsplumb instance
     *  _globals.jsPlumb should be initiated
     *  @link https://jsplumbtoolkit.com/doc/events.html
     */
    function addJSPlumbEventListeners() {
      /* Notification a Connection was established. */
      _globals.jsPlumb.bind('connection', function (info) {
        var portIds = getPortIdsFromInfo(info);
        /* listen to some mouse events */
        $(info.connection.canvas).on('mouseenter', function () {
          _helper.doHook('connectionMouseEnter', info.sourceId, portIds.outputPortId, info.targetId, portIds.inputPortId, this);
        }).on('mouseleave', function () {
          _helper.doHook('connectionMouseLeave', info.sourceId, portIds.outputPortId, info.targetId, portIds.inputPortId, this);
        });
      });

      /* Notification that a connection was detached by user.  */
      _globals.jsPlumb.bind('connectionDetached', function (info, e) {
        if (e !== undefined) {
          var portIds = getPortIdsFromInfo(info);
          publicObj.disconnect(info.sourceId, portIds.outputPortId, info.targetId, portIds.inputPortId, info.connection);
        }
      });

      /* Notification that an existing connection's source or target endpoint was dragged to some new location */
      _globals.jsPlumb.bind('connectionMoved', function (info, e) {
        if (e !== undefined) {
          publicObj.disconnect(info.originalSourceId, getPortIdFromEndPoint(info.originalSourceEndpoint), info.originalTargetId, getPortIdFromEndPoint(info.originalTargetEndpoint), info.connection);
        }

      });

      /* Notification a Connection was clicked. */
      _globals.jsPlumb.bind('click', function (info, originalEvent) {
        _helper.doHook('connectionClick', info.sourceId, getPortIdFromEndPoint(info.endpoints[0]), info.targetId, getPortIdFromEndPoint(info.endpoints[1]), info.connector.canvas);

      });

      /* Notification a Connection was double clicked. */
      _globals.jsPlumb.bind('dblclick', function (info, originalEvent) {
        _helper.doHook('connectionDblClick', info.sourceId, getPortIdFromEndPoint(info.endpoints[0]), info.targetId, getPortIdFromEndPoint(info.endpoints[1]), info.connector.canvas);
      });


      /* zoom has changed */
      _globals.jsPlumb.bind('zoom', function (info, originalEvent) {

      });

      /* Drag connection */
      _globals.jsPlumb.bind('connectionDrag', function (connection) {
        _globals.isDragging = true;

      });

      /* Drop connection */
      _globals.jsPlumb.bind('connectionDragStop', function (connection) {
        _globals.isDragging = false;

      });

      /* endpoint is dropped on another endpoint */
      _globals.jsPlumb.bind('beforeDrop', function (info) {
        var portIds = getPortIdsFromInfo(info);

        /* register connection, do not allow double coneections*/
        if (!publicObj.hasConnection(info.sourceId, portIds.outputPortId, info.targetId, portIds.inputPortId)) {
          publicObj.connect(info.sourceId, portIds.outputPortId, info.targetId, portIds.inputPortId, info.connection);
          return true;
        }
        return false;
      });
    }

    /**
     * Creates unique id
     **/
    function getUniqueId() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
      }
      return function () {
        return s4() + s4() + s4() + s4() +
                s4() + s4() + s4() + s4();
      }();
    }

    /* initialize krytWorkflow
     */
    if (_cdnFilesToBeLoaded.length === 0) {
      _helper.doInit();
    }
  };

  $.fn.krytWorkflow = function (settings) {
    return this.each(function () {
      if (undefined === $(this).data('krytWorkflow')) {
        var plugin = new $.krytWorkflow(this, settings);
        $(this).data('krytWorkflow', plugin);
      }
    });
  };

})(jQuery);