-- SUMMARY --
This module provides an overview table of ExperD activity by group for teachers. 

-- REQUIREMENTS --
ExperD Reports depends on the ExperD module.

-- INSTALLATION --
Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --
None needed

-- USAGE --
Create a new node of type "ExperD Teacher Reports".
