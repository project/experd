<?php
/**
 * @file
 * experd_report_feature.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function _experd_reports_installed_fields() {
  $field_bases = array();

  // Exported field_base: 'field_lb_select_experd'
  $field_bases['field_lb_select_experd'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_lb_select_experd',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'experd' => 'experd',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  return $field_bases;
}
