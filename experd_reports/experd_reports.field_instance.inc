<?php
/**
 * @file
 * experd_report_feature.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function _experd_reports_installed_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'node-teacher_experd_reports-field_lb_select_experd'
  $field_instances['node-teacher_experd_reports-field_lb_select_experd'] = array(
    'bundle' => 'teacher_experd_reports',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_lb_select_experd',
    'label' => 'Select Experd',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Select Experd');

  return $field_instances;
}
