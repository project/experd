<?php
/**
 * @file
 * experd.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function experd_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
}

/**
 * Implements hook_node_info().
 */
function experd_node_info() {
  $items = array(
    'experd' => array(
      'name' => t('ExperD'),
      'base' => 'node_content',
      'description' => t('An Experiment Designer'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'experd_group' => array(
      'name' => t('ExperD Group'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'experd_operation' => array(
      'name' => t('ExperD Operation'),
      'base' => 'node_content',
      'description' => t('An operation for the Experiment Designer, usually a laboratory method.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'experd_port' => array(
      'name' => t('ExperD Port'),
      'base' => 'node_content',
      'description' => t('Port definition for an ExperD operation.'),
      'has_title' => '1',
      'title_label' => t('Port name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
