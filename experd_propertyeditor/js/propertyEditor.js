(function () {
  /**
   * @ngdoc overview
   * @name propertyEditor
   * @description
   * #ExperD propertyEditor plugin
   * This is the AngularJS part of the propertyEditor plugin, part of the
   * experd_propertyeditor Drupal module.
   *
   * In the `.run()` function of this module, the plugin registers some event handlers
   * to hook into the ExperD workflow.
   *
   * This plugin listens for the following events:
   * - 'krytWorkflow.figureClick'
   *  When a figure in the workflow is clicked the plugin inserts an
   *  <xp-property-editor> directive into the pluginarea of the main-view.
   * - 'krytWorkflow.addFigure'
   *  When a figure is added to the workflow, the plugin adds a widget div to it,
   *  to display a small icon if a result text is added.
   * - 'plugin.update.propertyeditor'
   *   This event is emitted when the plugindata for this plugin is changed for an
   *   operation state in the workflow. The plugin can update the figure in the workflow
   *   e.g. a widget icon or name.
   *
   */
  angular.module('propertyEditor', [])
          .run(['$rootScope', 'ExperDOperationService', '$compile', '$modal',
            function ($rootScope, ExperDOperationService, $compile, $modal) {
              //console.log('propertyEditor plugin is active');

              function operationClickedHandler(event, uuid) {
                // find element to add plugin directive to
                var thePluginArea = angular.element('#pluginarea');//.html('uuid: ' + uuid + ' ');
                var scope = thePluginArea.scope(); // need scope to compile html directive
                // compile directive and add to pluginarea
                thePluginArea.html($compile('<xp-property-editor xp-uuid="' + uuid + '"/>')(scope));
              }

              // Handle click on operation in workflow
              $rootScope.$on('krytWorkflow.figureClick', operationClickedHandler);

              // Add widget div to operation in workflow
              $rootScope.$on('krytWorkflow.addFigure', function (event, nid, uuid, byGUI) {
                //angular.element("#" + uuid).find(".kw_figure_widgets").append('<div id="propertyeditor" class="propertyeditor results"></div>');
                angular.element("#" + uuid + " .kw_figure_widgets").append('<div id="propertyeditor" class="propertyeditor results"></div>');
              });

              //Update operation widget if data is updated
              $rootScope.$on("plugin.update.propertyeditor", function (event, uuid, pluginName, pluginData, oldPluginData) {
                //console.log(pluginName, pluginData, oldPluginData);
                if (pluginName === "propertyeditor") {
                  // Handle Result tab
                  if (pluginData && pluginData.result) {
                    // show icon and set tooltip if pluginData.result is set
                    angular.element("#" + uuid + " #propertyeditor.results").html('<i class="fa fa-comment fa-lg" title="' + pluginData.result + '"></i>').show();
                  } else {
                    // hide icon
                    angular.element("#" + uuid + " #propertyeditor.results").hide();
                  }

                  // Handle Custom Name
                  if (pluginData && pluginData.customName) {
                    // change name in figure if pluginData.customName is set
                    angular.element("#" + uuid + " .kw_figure_name").html(pluginData.customName);
                  } else {
                    // reset original name
                    var theState = ExperDOperationService.getStateByUuid(uuid);
                    var theOperation = ExperDOperationService.currentOperations[theState.operationDbNid];
                    angular.element("#" + uuid + " .kw_figure_name").html(theOperation.name);
                  }

                  // Handle Custom Image
                  if (pluginData && pluginData.customImage) {
                    // change name in figure if pluginData.customImage is set
                    angular.element("#" + uuid + " .kw_figure_image img").attr('src', pluginData.customImage.url);
                  }
                  else {
                    // reset original image
                    var theState = ExperDOperationService.getStateByUuid(uuid);
                    var theOperation = ExperDOperationService.currentOperations[theState.operationDbNid];
                    if (theOperation.imageUrl) {
                      angular.element("#" + uuid + " .kw_figure_image img").attr('src', theOperation.imageUrl);
                    }
                  }

                  // Handle operation status
                  if (pluginData && isFinite(pluginData.status)) {
                    var op = angular.element("#" + uuid);
                    switch (parseInt(pluginData.status, 10)) {
                      case 0:
                        op.removeClass('status_inprogress status_finished');
                        break;
                      case 1:
                        op.removeClass('status_finished').addClass('status_inprogress');
                        break;
                      case 2:
                        op.removeClass('status_inprogress').addClass('status_finished');
                        break;
                    }
                  }
                }
              });

            }]);

  /**
   * @ngdoc directive
   * @name propertyEditor.directive:xpPropertyEditor
   * @restrict E
   * @requires experdServices.ExperDOperationService
   * @description
   * Displays a property editor for selectes operation state in the workflow.
   * The propertyeditor displays an accordion widget with a number of accordion-groups.
   * The accordion is filled from an array with a list of titles and templates to load.
   * The first item in the list has the title of the operation and showa the
   * operations image and description. The other items are defined in the Drupal
   * plugin and passed in the Drupal.settings.
   *
   * @TODO Improvement: Create a directive for each tab-type?
   */
  angular.module('propertyEditor').directive('xpPropertyEditor', ['ExperDOperationService', 'ExperDAPI',
    function (ExperDOperationService, ExperDAPI) {
      // get template path from Drupal.settings
      var templatePath = Drupal.settings.basePath + Drupal.settings.propertyeditor.modulePath + '/js/templates/';
      // get array of templates to add to accordion from Drupal.settings
      // this gives other plugins the opportunity to add their templates to the list
      var drupalPlugins = Drupal.settings.propertyeditor.pluginForms;
      var plugins = [
        {
          'title': '',
          'templateUrl': templatePath + 'info.html'
        }
      ];

      var editName = [
        {
          'title': 'Edit operation properties',
          'templateUrl': templatePath + 'editName.html'
        }];

      return {
        restrict: 'E',
        templateUrl: templatePath + 'propertyEditorTemplate.html',
        scope: {}, // new scope
        /**
         *
         * @ngdoc controller
         * @name propertyEditor.controller:propertyEditorCtrl
         * @requires $scope
         * @description
         * Controller for xp-property-editor directive.
         */
        controller: function ($scope) {
          var self = this;

          function getDefaultUploadPath() {
            var xpnid = ExperDOperationService.xpnid;
            var groupId = 0;
            if (ExperDOperationService.getCurrentExpdGroups().length > 0) {
              groupId = ExperDOperationService.getCurrentExpdGroups()[0].nid;
            }
            return 'public://experd_upload/experd_' + xpnid + '/group_' + groupId + '/';
          }

          this.isEnabled_ChangeStatus = function () {
            return ExperDOperationService.currentExpd.enable_status;
          }
          /**
           * @ngdoc method
           * @name propertyEditor.controller:propertyEditorCtrl.SaveChanges
           * @methodOf propertyEditor.controller:propertyEditorCtrl
           * @function
           * @description
           * Saves the value entered in the result form to
           * the operationstate data.
           */
          this.SaveChanges = function () {
            ExperDOperationService.updateOperationStatePluginData($scope.uuid, 'propertyeditor', $scope.pluginData);//{'result': this.resultsTxt});
          };

          /**
           * @ngdoc method
           * @name propertyEditor.controller:propertyEditorCtrl.uploadImage
           * @methodOf propertyEditor.controller:propertyEditorCtrl
           * @function
           * @description
           * Upload an image file for the custom operation image
           */
          this.uploadImage = function () {
            self.image.filepath = getDefaultUploadPath() + $scope.uuid + '/' + self.image.filename;
            if ($scope.pluginData.customImage) {
              var oldFileId = $scope.pluginData.customImage.fid;
            }
            ExperDAPI.uploadFile(self.image)
                    .success(function (result) {
                      $scope.pluginData.customImage = result;
                      ExperDAPI.retrieveFile(result.fid)
                              .success(function (fileinfo) {
                                // Saving full uri could break url when server changes!
                                // Webservice will retrieve full url (server-side) based on fid.
                                $scope.pluginData.customImage.url = fileinfo.uri_full;
                                ExperDOperationService.updateOperationStatePluginData($scope.uuid, 'propertyeditor', $scope.pluginData);
                                if (oldFileId) {
                                  // Should we really delete the file here, or just the reference??
                                  // This invalidates references from older states from state_history
                                  ExperDAPI.deleteFile(oldFileId);
                                }
                              });
                    });
          };

          /**
           * @ngdoc method
           * @name propertyEditor.controller:propertyEditorCtrl.downloadFileByFid
           * @methodOf propertyEditor.controller:propertyEditorCtrl
           * @function
           * @param {int} fid Drupal managed file id
           * @description
           * Download a Drupal file with id fid.
           */
          this.downloadFileByFid = function (fid) {
            ExperDAPI.retrieveFile(fid)
                    .success(function (fileInfo) {
                      window.open(fileInfo.uri_full);
                    });
          };

          /**
           * @ngdoc method
           * @name propertyEditor.controller:propertyEditorCtrl.deleteResultsFile
           * @methodOf propertyEditor.controller:propertyEditorCtrl
           * @function
           * @param {object} attachment The attachment to delete.
           * @description
           * Delete the attachment from the results.
           */
          this.deleteResultsFile = function (attachment) {
            var index = $scope.pluginData.resultUploads.indexOf(attachment);
            if (index > -1) {
              // Should we really delete the file here, or just the reference??
              ExperDAPI.deleteFile(attachment.fid);
              $scope.pluginData.resultUploads.splice(index, 1);
              ExperDOperationService.updateOperationStatePluginData($scope.uuid, 'propertyeditor', $scope.pluginData);
            }
          };

          /**
           * @ngdoc method
           * @name propertyEditor.controller:propertyEditorCtrl.uploadResultsFile
           * @methodOf propertyEditor.controller:propertyEditorCtrl
           * @function
           * @description
           * Upload a file and add to the list of attachments for this operation state.
           */
          this.uploadResultsFile = function () {
            self.resultsfile.filepath = getDefaultUploadPath() + $scope.uuid + '/results/' + self.resultsfile.filename;
            ExperDAPI.uploadFile(self.resultsfile)
                    .success(function (result) {
                      if (!$scope.pluginData.resultUploads) {
                        $scope.pluginData.resultUploads = [];
                      }
                      $scope.pluginData.resultUploads.push({
                        "fid": result.fid,
                        "title": self.resultsfileTitle || self.resultsfile.filename
                      });
                      ExperDOperationService.updateOperationStatePluginData($scope.uuid, 'propertyeditor', $scope.pluginData);
//                              ExperDAPI.retrieveFile(result.fid)
//                                      .success(function (fileinfo) {
//                                        if (!$scope.pluginData.resultUploads) {
//                                          $scope.pluginData.resultUploads = [];
//                                        }
//                                        $scope.pluginData.resultUploads.push({
//                                          "fid": result.fid,
//                                          "url": fileinfo.uri_full
//                                        });
//                                        ExperDOperationService.updateOperationStatePluginData($scope.uuid, 'propertyeditor', $scope.pluginData);
//                                      });
                    });
          };
        },
        controllerAs: 'propertyEditorCtrl',
        link: function (scope, element, attrs, ctrlr) {
          // store uuid, state and operation on the scope so they can be
          // accessed from the templates
          scope.uuid = attrs.xpUuid;
          scope.state = ExperDOperationService.getStateByUuid(scope.uuid);
          scope.operation = ExperDOperationService.currentOperations[scope.state.operationDbNid];
          //scope.groupMembers = ExperDOperationService.getCurrentExpdGroups()[0].members;
          // get the plugindata for this plugin
          scope.pluginData = ExperDOperationService.getOperationStatePluginData(scope.uuid, 'propertyeditor');

          if (!scope.pluginData) { // initialize plugindata if undefined
            scope.pluginData = {};
          }

          var currentGroups = ExperDOperationService.getCurrentExpdGroups();
          var groupMembers = angular.isArray(currentGroups) && currentGroups.length>0 ? currentGroups[0].members : [];
          if (!scope.pluginData.groupMembers) {
            // initialize if no groupmembers are listed
            scope.pluginData.groupMembers = angular.copy(groupMembers);
			ctrlr.SaveChanges();
          } else if (scope.pluginData.groupMembers.length !== groupMembers.length) {
            // number of members changed
            // reset members but keep selected state
            var oldMembers = scope.pluginData.groupMembers;
            scope.pluginData.groupMembers = angular.copy(groupMembers);
            angular.forEach(scope.pluginData.groupMembers, function (value, key, obj) {
              if (oldMembers[key] && oldMembers[key].selected === true) {
                value.selected = true;
              }
            });
            ctrlr.SaveChanges();
          }

          if (!scope.pluginData.outputPorts) {
            scope.pluginData.outputPorts = angular.copy(scope.operation.outputPorts);
            ctrlr.SaveChanges();
          }

          if (!isFinite(scope.pluginData.status)) {
            scope.pluginData.status = 0;
            ctrlr.SaveChanges();
          }

          //scope.pluginForms = plugins;
          // set title of first item to operation name
          plugins[0].title = scope.pluginData.customName || scope.operation.name;
          // add items from drupal to pluginForms array
          if (scope.operation.classification.name == "Custom" || scope.operation.nid < 0) {
            scope.pluginForms = plugins.concat(drupalPlugins, editName);
          } else {
            scope.pluginForms = plugins.concat(drupalPlugins);
          }

          /**
           * The selected state can also be changed or deleted by the
           * user, another plugin or from another client working on
           * the same workflow.
           * Watch for changes, so they can be updated in the property
           * editor when needed.
           */
          var unwatch = scope.$watch(
                  function () { // watch operation state loaded in property editor for changes
                    return ExperDOperationService.getStateByUuid(scope.uuid);
                  },
                  function (value, oldValue) { // state changed?
                    //scope.state = value;
                    if (!value || value.databaseAction === 'delete' || value.plugins.experd.action === 'delete') {
                      // figure is deleted
                      angular.element('#pluginarea').empty();
                    } else {
                      if (value.plugins.propertyeditor) {
                        // update value
                        scope.pluginData = value.plugins.propertyeditor;
                        plugins[0].title = scope.pluginData.customName || scope.operation.name;
                      }
                    }
                  }, true);

          element.on('$destroy', function () {
            unwatch();
          });

        }
      };

    }]);

})();