<!DOCTYPE html>
<html>

<head>
  <title><?php print $title; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="<?php print $modulepath; ?>/app/favicon.ico" />
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body>
<?php
if (!empty($page['message'])):
  foreach($page['message'] as $type => $message):
    ?>
    <div class="messages <?php print $type; ?>">
      <ul>
        <?php foreach($message as $msg): ?>
          <li><?php print $msg; ?></li>
        <?php endforeach; ?>
      </ul>
    </div>
  <?php
  endforeach;
endif; ?>

<?php print $content; ?>
</body>
</html>