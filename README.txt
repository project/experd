-- A. INSTALLATION --
Install ExperD CE module as desribed at https://www.drupal.org/documentation/install/modules-themes.

-- B. CONFIGURATION --
Configure Organic Groups for ExperD CE:

1. Configure ExperD Group as Organic Group:
- Go to Structure->ContentTypes->ExperD Group
- Select tab "Organic Groups", Select checkbox "Group"
- Click "Save content type"

2. Configure ExperD as "Group Content":
- Go to Structure->ContentTypes->ExperD
- Select tab "Organic Groups", Select checkbox "Group content"
- Click "Save content type"

3. Configure "Group content visibility" for "ExperD"
- Go to Admin->Configuration->Organic groups->OG field settings
- Under "Bundles", select "ExperD"
- Under "Fields", select "Group content visibility"
- Click "Add field" button

4. Configure "Group visibility" for "ExperD Group"
- Go to Admin->Configuration->Organic groups->OG field settings
- Under "Bundles", select "ExperD Group"
- Under "Fields", select "Group visibility"
- Click "Add field" button

5. Go to Admin->Configuration->Organic groups->OG settings
- Unselect option: "Strict node access permissions"
- Click "Save configuration"

6. Update ExperD and ExperD Groups created before changing the OG configuration settings:

6.1. For ExperD:
- Go to ExperD Edit screen
- Select a value for "Group content visibility" (typically select "Private")
- Click "Save"

6.2. For ExperD Group:
- Go to "ExperD Group" edit screen
- Select Checkbox "Group"
- Select an option for Group visibility, depending on use case (self enroll groups (Public) or pre-defined groups (Private))
- Click "Save"

-- C. PERFORMANCE --
ExperD CE uses polling for syncing workflows between users. Please consider the following recommendations for optimal performance.

1. In case of Apache
- Increase ServerLimit and MaxClients (http://httpd.apache.org/docs/2.2/mod/worker.html). The optimal values for these settings dpeend on the amount of RAM/CPU of your server.
- Increase server RAM to 32 or even 64 GB

2. Consider using NGINX (http://nginx.org/en/) instead of Apache.

3. Use a Redis Drupal module for database locking, e.g. https://github.com/omega8cc/redis.

4. Consider using a load balancer / cluster setup.