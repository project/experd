<?php

/**
 * @file 
 * Install file for Experiment Designer
 */

/**
 * Implements hook_schema()
 * Create database tables:
 *  - labbuddy_state
 *  - labbuddy_state_history
 *  - labbuddy_experd_state
 *  - labbuddy_experd_state_history
 * 
 * @return array
 */
function experd_schema() {

  $schema['lb_state'] = array(
    'description' => 'LabBuddy state table, all active states',
    'fields' => array(
      'operation_uuid' => array(
        'description' => 'Unique operation id',
        'type' => 'char',
        'length' => '32',
        'not null' => TRUE,
      ),
      'sid' => array(
        'description' => 'State id, updated when state is changed',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'entity_nid' => array(
        'description' => 'ExperD entity node id',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'operation_nid' => array(
        'description' => 'ExperD operation node id',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'gid' => array(
        'description' => 'OG group id',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'uid' => array(
        'description' => 'User id',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'timestamp' => array(
        'description' => 'Last updated',
        'type' => 'int',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('operation_uuid'),
    'unique keys' => array(
      'loadGroupScheme' => array('entity_nid', 'gid', 'sid'),
      'sid' => array('sid')
    ),
  );

  $schema['lb_state_history'] = array(
    'description' => 'Keeps history (copies) of all state changes',
    'fields' => array(
      'sid' => array(
        'description' => 'State id, updated when state is changed',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'operation_uuid' => array(
        'description' => 'Unique operation id',
        'type' => 'char',
        'length' => '32',
        'not null' => FALSE,
      ),
      'entity_nid' => array(
        'description' => 'ExperD entity node id',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'operation_nid' => array(
        'description' => 'ExperD operation node id',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'gid' => array(
        'description' => 'OG group id',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'uid' => array(
        'description' => 'User id',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'deleted' => array(
        'description' => '1 if operation has been deleted',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => FALSE,
        'default' => 0,
      ),
      'timestamp' => array(
        'description' => 'Last updated',
        'type' => 'int',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('sid'),
    'indexes' => array(
      'getDeleted' => array('gid', 'entity_nid', 'deleted', 'sid'),
    ),
  );

  $schema['lb_experd_state'] = array(
    'description' => 'State table for Experd plugin data',
    'fields' => array(
      'fk_sid' => array(
        'description' => 'Experd plugin data for labbbuddy_state sid',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'operation_uuid' => array(
        'description' => 'Unique operation id',
        'type' => 'char',
        'length' => '32',
        'not null' => FALSE,
      ),
      'action' => array(
        'description' => 'gives clue about what data changed, e.g. \'location\'',
        'type' => 'varchar',
        'length' => '10',
        'not null' => FALSE,
      ),
      'location' => array(
        'description' => 'JSON string representing location of operation',
        'type' => 'varchar',
        'length' => '45',
        'not null' => FALSE,
      ),
      'dimensions' => array(
        'description' => 'JSON string representing dimensions of operation',
        'type' => 'varchar',
        'length' => '45',
        'not null' => FALSE,
      ),
      'connections' => array(
        'description' => 'JSON string representing connections of operation',
        'type' => 'varchar',
        'length' => '1024',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('fk_sid'),
    'unique keys' => array(
      'uuid' => array('operation_uuid')
    ),
  );

  $schema['lb_experd_state_history'] = array(
    'description' => 'Keeps history (copies) of all experd_state changes',
    'fields' => array(
      'fk_sid' => array(
        'description' => 'Experd plugin data for labbbuddy_state sid',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'operation_uuid' => array(
        'description' => 'Unique operation id',
        'type' => 'char',
        'length' => '32',
        'not null' => FALSE,
      ),
      'action' => array(
        'description' => 'gives clue about what data changed, e.g. \'location\'',
        'type' => 'varchar',
        'length' => '10',
        'not null' => FALSE,
      ),
      'location' => array(
        'description' => 'JSON string representing location of operation',
        'type' => 'varchar',
        'length' => '45',
        'not null' => FALSE,
      ),
      'dimensions' => array(
        'description' => 'JSON string representing dimensions of operation',
        'type' => 'varchar',
        'length' => '45',
        'not null' => FALSE,
      ),
      'connections' => array(
        'description' => 'JSON string representing connections of operation',
        'type' => 'varchar',
        'length' => '1024',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('fk_sid'),
  );

  $schema['lb_schema_snapshots'] = array(
    'description' => 'Store a snapshot as a list of sids.',
    'fields' => array(
      'snap_id' => array(
        'description' => 'Unique id of this snapshot',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'Name or label of this snapshot',
        'type' => 'char',
        'length' => '255',
        'not null' => TRUE,
      ),
      'entity_nid' => array(
        'description' => 'Node id of the experd this snapshot belongs to',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'gid' => array(
        'description' => 'Group id of the group that saved this snapshot',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => 'user id of the user who saved this snapshot',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'sids' => array(
        'description' => 'List of state ids, saved as JSON',
        'type' => 'text',
        'not null' => TRUE,
      ),
      'timestamp' => array(
        'description' => 'Timestamp',
        'type' => 'int',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('snap_id'),
  );

  return $schema;
}

function experd_update_7000() {
  $schema['experd_savedoperation'] = array(
    'description' => t('Saved operation.'),
    'fields' => array(
      'sid' => array(
        'description' => t('Save id.'),
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'experd_nid' => array(
        'description' => t('Experiment designer node id'),
        'type' => 'int',
        'not null' => TRUE,
      ),
      'experd_groupId' => array(
        'description' => t('Experiment Designer Group id.'),
        'type' => 'int',
        'not null' => TRUE,
      ),
      'experd_operation_db_nid' => array(
        'description' => t('Experiment Designer Operation database node id.'),
        'type' => 'int',
        'not null' => TRUE,
      ),
      'experd_operation_js_nid' => array(
        'description' => t('Experiment Designer Operation Javascript node id.'),
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'action' => array(
        'description' => t('Action (1=add, 2=update, 3=delete)'),
        'type' => 'int',
        'not null' => TRUE,
      ),
      'formValues' => array(
        'description' => t('The form values'),
        'type' => 'text',
        'not null' => TRUE,
      ),
      'userData' => array(
        'description' => t('Extra data'),
        'type' => 'text',
        'not null' => TRUE,
      ),
      'location' => array(
        'description' => t('The location of the operation'),
        'type' => 'text',
        'not null' => TRUE,
      ),
      'dimensions' => array(
        'description' => t('The dimensions of the operation'),
        'type' => 'text',
        'not null' => TRUE,
      ),
      'connections' => array(
        'description' => t('The ingoing connections'),
        'type' => 'text',
        'not null' => TRUE,
      ),
      'startDatetime' => array(
        'description' => t('Start timestamp of this operation in schedule'),
        'type' => 'int',
        'not null' => TRUE,
      ),
      'endDatetime' => array(
        'description' => t('End timestamp of this operation in schedule'),
        'type' => 'int',
        'not null' => TRUE,
      ),
      'sampleCount' => array(
        'description' => t('Number of samples'),
        'type' => 'int',
        'not null' => TRUE,
      ),
      'obsolete' => array(
        'description' => t('Obsolete boolean. If true, this save can ignored.'),
        'type' => 'int',
        'not null' => TRUE,
      ),
      'previous_sid' => array(
        'description' => t('The sid of the previous savedoperation of this operation.'),
        'type' => 'int',
        'not null' => TRUE,
      ),
      'timestamp' => array(
        'description' => t('Timestamp of this savedoperation'),
        'type' => 'int',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('sid'),
    'indexes' => array(
      'basic' => array('sid', 'experd_nid', 'experd_groupId', 'experd_operation_db_nid', 'obsolete'),
    ),
  );

  db_create_table('experd_savedoperation', $schema['experd_savedoperation']);
}

/**
 * added userId field to experd_savedoperation
 */
function experd_update_7001() {
  $field = array(
    'description' => t('Id of user that saved this saveoperation.'),
    'type' => 'int',
    'initial' => 0,
    'not null' => TRUE,
  );
  db_add_field('experd_savedoperation', 'userId', $field);
}

/**
 * new tables for experd
 * table experd_savedoperation is not used anymore
 */
function experd_update_7002() {
  $schema['labbuddy_state'] = array(
    'description' => 'LabBuddy state table, all active states',
    'fields' => array(
      'operation_uuid' => array(
        'description' => 'Unique operation id',
        'type' => 'char',
        'length' => '32',
        'not null' => TRUE,
      ),
      'sid' => array(
        'description' => 'State id, updated when state is changed',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'entity_nid' => array(
        'description' => 'ExperD entity node id',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'operation_nid' => array(
        'description' => 'ExperD operation node id',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'groupId' => array(
        'description' => 'OG group id',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'uid' => array(
        'description' => 'User id',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'timestamp' => array(
        'description' => 'Last updated',
        'type' => 'int',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('operation_uuid'),
    'unique keys' => array(
      'loadGroupScheme' => array('entity_nid', 'groupId', 'sid'),
      'sid' => array('sid')
    ),
  );

  $schema['labbuddy_state_history'] = array(
    'description' => 'Keeps history (copies) of all state changes',
    'fields' => array(
      'sid' => array(
        'description' => 'State id, updated when state is changed',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'operation_uuid' => array(
        'description' => 'Unique operation id',
        'type' => 'char',
        'length' => '32',
        'not null' => FALSE,
      ),
      'entity_nid' => array(
        'description' => 'ExperD entity node id',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'operation_nid' => array(
        'description' => 'ExperD operation node id',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'groupId' => array(
        'description' => 'OG group id',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'uid' => array(
        'description' => 'User id',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'deleted' => array(
        'description' => '1 if operation has been deleted',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => FALSE,
        'default' => 0,
      ),
      'timestamp' => array(
        'description' => 'Last updated',
        'type' => 'int',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('sid'),
    'indexes' => array(
      'getDeleted' => array('groupId', 'entity_nid', 'deleted', 'sid'),
    ),
  );

  $schema['labbuddy_experd_state'] = array(
    'description' => 'State table for Experd plugin data',
    'fields' => array(
      'fk_sid' => array(
        'description' => 'Experd plugin data for labbbuddy_state sid',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'operation_uuid' => array(
        'description' => 'Unique operation id',
        'type' => 'char',
        'length' => '32',
        'not null' => FALSE,
      ),
      'action' => array(
        'description' => 'gives clue about what data changed, e.g. \'location\'',
        'type' => 'varchar',
        'length' => '10',
        'not null' => FALSE,
      ),
      'location' => array(
        'description' => 'JSON string representing location of operation',
        'type' => 'varchar',
        'length' => '45',
        'not null' => FALSE,
      ),
      'dimensions' => array(
        'description' => 'JSON string representing dimensions of operation',
        'type' => 'varchar',
        'length' => '45',
        'not null' => FALSE,
      ),
      'connections' => array(
        'description' => 'JSON string representing connections of operation',
        'type' => 'varchar',
        'length' => '1024',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('fk_sid'),
    'unique keys' => array(
      'uuid' => array('operation_uuid')
    ),
  );

  $schema['labbuddy_experd_state_history'] = array(
    'description' => 'Keeps history (copies) of all experd_state changes',
    'fields' => array(
      'fk_sid' => array(
        'description' => 'Experd plugin data for labbbuddy_state sid',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'operation_uuid' => array(
        'description' => 'Unique operation id',
        'type' => 'char',
        'length' => '32',
        'not null' => FALSE,
      ),
      'action' => array(
        'description' => 'gives clue about what data changed, e.g. \'location\'',
        'type' => 'varchar',
        'length' => '10',
        'not null' => FALSE,
      ),
      'location' => array(
        'description' => 'JSON string representing location of operation',
        'type' => 'varchar',
        'length' => '45',
        'not null' => FALSE,
      ),
      'dimensions' => array(
        'description' => 'JSON string representing dimensions of operation',
        'type' => 'varchar',
        'length' => '45',
        'not null' => FALSE,
      ),
      'connections' => array(
        'description' => 'JSON string representing connections of operation',
        'type' => 'varchar',
        'length' => '1024',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('fk_sid'),
  );

  db_create_table('labbuddy_state', $schema['labbuddy_state']);
  db_create_table('labbuddy_state_history', $schema['labbuddy_state_history']);
  db_create_table('labbuddy_experd_state', $schema['labbuddy_experd_state']);
  db_create_table('labbuddy_experd_state_history', $schema['labbuddy_experd_state_history']);
}

  /**
   * update "groupID" to "gid" in labbuddy_state(_history) tables.
   * Also updates unique keys and indexes with "groupId" to "gid"
   */
function experd_update_7003() {
  db_drop_unique_key('labbuddy_state', 'loadGroupScheme');
  db_change_field('labbuddy_state', 'groupId', 'gid', array(
    'description' => 'OG group id',
    'type' => 'int',
    'not null' => FALSE,
      ), array(
    'unique keys' => array(
      'loadGroupScheme' => array('entity_nid', 'gid', 'sid'),
    ),
  ));

  db_drop_index('labbuddy_state_history', 'getDeleted');
  db_change_field('labbuddy_state_history', 'groupId', 'gid', array(
    'description' => 'OG group id',
    'type' => 'int',
    'not null' => FALSE,
      ), array(
    'indexes' => array(
      'getDeleted' => array('gid', 'entity_nid', 'deleted', 'sid'),
    ),
  ));
}

/**
 * Add table to store schema snapshots
 */
function experd_update_7004(){
$schema['labbuddy_schema_snapshots'] = array(
    'description' => 'Store a snapshot as a list of sids.',
    'fields' => array(
      'snap_id' => array(
        'description' => 'Unique id of this snapshot',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'Name or label of this snapshot',
        'type' => 'char',
        'length' => '255',
        'not null' => TRUE,
      ),
      'entity_nid' => array(
        'description' => 'Node id of the experd this snapshot belongs to',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'gid' => array(
        'description' => 'Group id of the group that saved this snapshot',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => 'user id of the user who saved this snapshot',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'sids' => array(
        'description' => 'List of state ids, saved as JSON',
        'type' => 'text',
        'not null' => TRUE,
      ),
      'timestamp' => array(
        'description' => 'Timestamp',
        'type' => 'int',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('snap_id'),
  );
  
  db_create_table('labbuddy_schema_snapshots', $schema['labbuddy_schema_snapshots']);
}

/**
 * Rename "labbuddy_???" tables to "lb_???"
 */
function experd_update_7005(){
  db_rename_table('labbuddy_state', 'lb_state');
  db_rename_table('labbuddy_state_history', 'lb_state_history');
  db_rename_table('labbuddy_experd_state', 'lb_experd_state');
  db_rename_table('labbuddy_experd_state_history', 'lb_experd_state_history');
  db_rename_table('labbuddy_schema_snapshots', 'lb_schema_snapshots');
}