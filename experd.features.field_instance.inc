<?php
/**
 * @file
 * experd.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function experd_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-experd-field_xd_courses'.
  $field_instances['node-experd-field_xd_courses'] = array(
    'bundle' => 'experd',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select courses for this ExperD. Courses are specified in the vocabulary "Courses".',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 10,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_xd_courses',
    'label' => 'Courses',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-experd-field_xd_description'.
  $field_instances['node-experd-field_xd_description'] = array(
    'bundle' => 'experd',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_xd_description',
    'label' => 'Description',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-experd-field_xd_enable_status'.
  $field_instances['node-experd-field_xd_enable_status'] = array(
    'bundle' => 'experd',
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => 'Students can set the status of an operation to "None" / "In progress" / "Finished".',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 14,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_xd_enable_status',
    'label' => 'Allow students to change operation status',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-experd-field_xd_freeze_workflow'.
  $field_instances['node-experd-field_xd_freeze_workflow'] = array(
    'bundle' => 'experd',
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => 'The workflow will be read-only when it is submitted for approval.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_key',
        'weight' => 11,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_xd_freeze_workflow',
    'label' => 'Freeze submitted workflow',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-experd-field_xd_operations'.
  $field_instances['node-experd-field_xd_operations'] = array(
    'bundle' => 'experd',
    'deleted' => 0,
    'description' => 'Select the operations students can use in this designer.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_xd_operations',
    'label' => 'ExperD Operations',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'multiselect',
      'settings' => array(),
      'type' => 'multiselect',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-experd-field_xd_users'.
  $field_instances['node-experd-field_xd_users'] = array(
    'bundle' => 'experd',
    'deleted' => 0,
    'description' => 'Select which users (not in a group) use this designer.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 8,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_xd_users',
    'label' => 'Users',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'multiselect',
      'settings' => array(),
      'type' => 'multiselect',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-experd_group-field_xd_operations'.
  $field_instances['node-experd_group-field_xd_operations'] = array(
    'bundle' => 'experd_group',
    'deleted' => 0,
    'description' => 'Select the operations students from this group can use in designers.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_xd_operations',
    'label' => 'ExperD Operations',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'multiselect',
      'settings' => array(),
      'type' => 'multiselect',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-experd_group-group_group'.
  $field_instances['node-experd_group-group_group'] = array(
    'bundle' => 'experd_group',
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => 'Determine if this is an OG group.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'og_ui',
        'settings' => array(
          'field_name' => FALSE,
        ),
        'type' => 'og_group_subscribe',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'display_label' => 1,
    'entity_type' => 'node',
    'field_name' => 'group_group',
    'label' => 'Group',
    'required' => FALSE,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'view modes' => array(
      'full' => array(
        'custom settings' => FALSE,
        'label' => 'Full',
        'type' => 'og_group_subscribe',
      ),
      'teaser' => array(
        'custom settings' => FALSE,
        'label' => 'Teaser',
        'type' => 'og_group_subscribe',
      ),
    ),
    'widget' => array(
      'module' => 'options',
      'settings' => array(
        'display_label' => 0,
        'og_hide' => TRUE,
      ),
      'type' => 'options_onoff',
      'weight' => 0,
    ),
  );

  // Exported field_instance: 'node-experd_operation-field_xd_classification'.
  $field_instances['node-experd_operation-field_xd_classification'] = array(
    'bundle' => 'experd_operation',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select the category this operation belongs to.<br>
The categories are specified in a Taxonomy glossary: Classification',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_xd_classification',
    'label' => 'Classification',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-experd_operation-field_xd_image'.
  $field_instances['node-experd_operation-field_xd_image'] = array(
    'bundle' => 'experd_operation',
    'deleted' => 0,
    'description' => 'The image to use for this operation in the design view.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_xd_image',
    'label' => 'Image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => 'experd_operation',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-experd_operation-field_xd_labbuddy_methods'.
  $field_instances['node-experd_operation-field_xd_labbuddy_methods'] = array(
    'bundle' => 'experd_operation',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select a related Web Lab Manual method. ',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 6,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_xd_labbuddy_methods',
    'label' => 'Related Web Lab Manual methods',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-experd_operation-field_xd_more_info_url'.
  $field_instances['node-experd_operation-field_xd_more_info_url'] = array(
    'bundle' => 'experd_operation',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Optional link to external website or field with more information about this operation.<br>Start url with protocol (e.g. http:// or https://) for links to external sites.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 9,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_xd_more_info_url',
    'label' => 'URL',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'node-experd_operation-field_xd_operation_description'.
  $field_instances['node-experd_operation-field_xd_operation_description'] = array(
    'bundle' => 'experd_operation',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Add a short description for this operation.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 8,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_xd_operation_description',
    'label' => 'Description',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-experd_operation-field_xd_ports'.
  $field_instances['node-experd_operation-field_xd_ports'] = array(
    'bundle' => 'experd_operation',
    'deleted' => 0,
    'description' => 'Add or delete ports to this operation.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_xd_ports',
    'label' => 'Ports',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'inline_entity_form',
      'settings' => array(
        'fields' => array(),
        'type_settings' => array(
          'allow_existing' => 0,
          'delete_references' => 1,
          'label_plural' => 'ports',
          'label_singular' => 'port',
          'match_operator' => 'CONTAINS',
          'override_labels' => 1,
        ),
      ),
      'type' => 'inline_entity_form',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-experd_port-field_xd_port_type'.
  $field_instances['node-experd_port-field_xd_port_type'] = array(
    'bundle' => 'experd_port',
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => 'Select the port type.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_xd_port_type',
    'label' => 'Port type',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Add a short description for this operation.');
  t('Add or delete ports to this operation.');
  t('Allow students to change operation status');
  t('Classification');
  t('Courses');
  t('Description');
  t('Determine if this is an OG group.');
  t('ExperD Operations');
  t('Freeze submitted workflow');
  t('Group');
  t('Image');
  t('Optional link to external website or field with more information about this operation.<br>Start url with protocol (e.g. http:// or https://) for links to external sites.');
  t('Port type');
  t('Ports');
  t('Related Web Lab Manual methods');
  t('Select a related Web Lab Manual method. ');
  t('Select courses for this ExperD. Courses are specified in the vocabulary "Courses".');
  t('Select the category this operation belongs to.<br>
The categories are specified in a Taxonomy glossary: Classification');
  t('Select the operations students can use in this designer.');
  t('Select the operations students from this group can use in designers.');
  t('Select the port type.');
  t('Select which users (not in a group) use this designer.');
  t('Students can set the status of an operation to "None" / "In progress" / "Finished".');
  t('The image to use for this operation in the design view.');
  t('The workflow will be read-only when it is submitted for approval.');
  t('URL');
  t('Users');

  return $field_instances;
}
