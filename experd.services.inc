<?php
/**
 * @file
 * experd.services.inc
 */

/**
 * Implements hook_default_services_endpoint().
 */
function experd_default_services_endpoint() {
  $export = array();

  $endpoint = new stdClass();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'experd_api';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'experd_api';
  $endpoint->authentication = array(
    'services' => 'services',
  );
  $endpoint->server_settings = array(
    'formatters' => array(
      'bencode' => TRUE,
      'json' => TRUE,
      'php' => TRUE,
      'xml' => TRUE,
      'yaml' => TRUE,
      'jsonp' => FALSE,
    ),
    'parsers' => array(
      'application/json' => TRUE,
      'application/vnd.php.serialized' => TRUE,
      'application/x-www-form-urlencoded' => TRUE,
      'application/x-yaml' => TRUE,
      'application/xml' => TRUE,
      'multipart/form-data' => TRUE,
      'text/xml' => TRUE,
    ),
  );
  $endpoint->resources = array(
    'experd' => array(
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
        ),
        'index' => array(
          'enabled' => '1',
        ),
      ),
      'relationships' => array(
        'operations' => array(
          'enabled' => '1',
        ),
        'groups' => array(
          'enabled' => '1',
        ),
        'getStatesFromSid' => array(
          'enabled' => '1',
        ),
        'getSchemaSnapshotById' => array(
          'enabled' => '1',
        ),
        'restoreSchemaSnapshotById' => array(
          'enabled' => '1',
        ),
        'getSchemaSnapshots' => array(
          'enabled' => '1',
        ),
      ),
      'targeted_actions' => array(
        'addOperationState' => array(
          'enabled' => '1',
        ),
        'saveSnapshot' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'file' => array(
      'operations' => array(
        'create' => array(
          'enabled' => '1',
        ),
        'retrieve' => array(
          'enabled' => '1',
        ),
        'delete' => array(
          'enabled' => '1',
        ),
      ),
      'actions' => array(
        'create_raw' => array(
          'enabled' => '1',
        ),
      ),
    ),
  );
  $endpoint->debug = 0;
  $export['experd_api'] = $endpoint;

  return $export;
}
