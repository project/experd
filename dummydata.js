[
  {
    "sid": 12,  // int
    "operationJsNid": "1234567890ABCDEF1234567890ABCDEF", //char(32)
    "operationDbNid": 69, //int
    "databaseAction": 'add||delete', // only when adding or deleting operation
    "plugins": {
      "experd": {
        "action": "add||delete||move||connect||disconnet",  //char(10)
        "location": {     //char(45)
          "x": 200,
          "y": 180
        },
        "dimensions": {   //char(45)
          "width": 100,
          "height": 80
        },
        "connectionsOut": [ //char(1024)
          {
            "sourcePortId": "string",
            "targetOperationJsNid": "string",
            "targetPortId": "string"
          }
        ]
      },
      "myplugin": {
        "myplugindata":{}
      }
    }
  }
];
